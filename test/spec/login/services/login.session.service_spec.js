'use strict';

describe('Login: SessionService', () => {
  var SessionService;
  var dummyUser;
  var put = sinon.spy();
  var get = sinon.spy();
  var remove = sinon.spy();

  beforeEach(module('mlaApp'));

  beforeEach(module({$cookies: {
    put: put,
    get: get,
    remove: remove
  }}));

  beforeEach(inject((_SessionService_) => {
    SessionService = _SessionService_;

    dummyUser = {
      username: 'dummy name',
      sessionId: 1,
      userId: 1,
      userRole: 'B'
    };

  }));

  it('should create the session using the user data', () => {
    SessionService.create(dummyUser);

    expect(SessionService.getUserId()).to.equal(1);
    expect(SessionService.getUsername()).to.equal('dummy name');
    expect(SessionService.getUserRole()).to.equal('B');

    assert(put.calledWith('session', angular.toJson(dummyUser)));
  });

  it('should not validate a user when the session is not created', () => {
    expect(SessionService.userLoggedIn()).to.be.false;
  });

  it('should validate a user when the session is created', () => {
    SessionService.create(dummyUser);
    expect(SessionService.userLoggedIn()).to.be.true;
  });

  it('should restore the session from the cookie', () => {
    SessionService.restore();
    assert(put.calledWith('session'));
  });

  it('should destroy session data', () => {
    SessionService.destroy();
    expect(SessionService.getUserId()).to.be.undefined;
    assert(remove.calledWith('session'));
  });

});
