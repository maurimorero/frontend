'use strict';

describe('Login: controller', () => {
  var createController;
  var loginStub;
  var $q;

  beforeEach(module('mlaApp'));

  beforeEach(inject(($injector, $controller) => {
    $q = $injector.get('$q');

    const deferred = $q.defer();
    const promise = deferred.promise;

    // Force promise resolve method (no catch is executed)
    sinon.stub(deferred, 'resolve').returns({then() {}});

    loginStub = sinon.stub().returns(promise);
    const AuthenticationService = {login: loginStub};

    createController = () => $controller('LoginController', {
      AuthenticationService
    });
  }));

  it('should call the init method', () => {
    const LoginController = createController();
    expect(LoginController.dataLoading).to.be.false;
  });

  it('should call AuthenticationService.login method when login controller method is executed', () => {
    const LoginController = createController();
    LoginController.login();
    assert(loginStub.called);
  });
});