'use strict';

describe('User: UserService', () => {
  var $httpBackend;
  var UserService;
  var UserModel;
  var API;
  var userData;

  beforeEach(module('mlaApp'));

  beforeEach(inject(($injector, _UserService_, _UserModel_, _API_) => {
    $httpBackend = $injector.get('$httpBackend');
    UserService = _UserService_;
    UserModel = _UserModel_;
    API = _API_;

    // API mock
    API.getUser = _.constant('/api/v1/user/1/?format=json');
    API.updateUser = _.constant('/api/v1/user/profile/1/');

    userData = {
      'area_code': 5000,
      'birthdate': '2016-11-16T03:00:00.000Z',
      'city': 'Córdoba',
      'cuit': 123,
      'document': 3123445,
      'first_name': 'Matiasss',
      'floor': 12,
      'gender': 1,
      'last_name': 'Debard',
      'phone': 545667,
      'street': 'some street',
      'street_number': 4,
      'unit': '12334'
    };

    // workaround for unexpected home html get
    $httpBackend
      .when('GET', 'app/home/views/home.view.html')
      .respond('');

    $httpBackend.flush();
  }));

  afterEach(() => {

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  it('should return a user instance from the server', () => {
    $httpBackend
      .when('GET', API.getUser)
      .respond(userData);

    UserService
      .getUser()
      .then(response => {
        expect(response).to.deep.equal(UserModel(userData));
      });

    $httpBackend.flush();
  });

  it('should create a new user', () => {
     $httpBackend
      .when('POST', API.register)
      .respond({status: 'user created'});

    UserService
      .createUser(userData)
      .then(response => {
        expect(response.data.status).to.equal('user created');
      });

    $httpBackend.flush();
  });

  it('should create a new user', () => {
    $httpBackend
      .when('POST', API.updateUser)
      .respond({status: 'user updated'});

    UserService
      .updateUser(userData)
      .then(response => {
        expect(response.data.status).to.equal('user updated');
      });

    $httpBackend.flush();
  });

});