(function () {
'use strict';

  angular
    .module('mlaApp')
    .config(configure)
    .run(onStart);

    /**
     * General configration
     */

    configure.$inject = [
      '$httpProvider',
      'localStorageServiceProvider',
      'cfpLoadingBarProvider',
      '$locationProvider',
      '$urlMatcherFactoryProvider'
    ];

    function configure($httpProvider, localStorageServiceProvider, cfpLoadingBarProvider, $locationProvider, $urlMatcherFactoryProvider) {
      $locationProvider.html5Mode(true);
      $urlMatcherFactoryProvider.strictMode(false);

      localStorageServiceProvider
        .setPrefix('mlaApp')
        .setStorageType('localStorage');

      cfpLoadingBarProvider.includeSpinner = false;
    }


    /**
     * App init configuration
     */

    onStart.$inject = ['$rootScope', '$state', '$location', '$window', 'AuthenticationService', 'localStorageService'];

    function onStart($rootScope, $state, $location, $window, AuthenticationService, localStorageService) {

      AuthenticationService.restoreCredentials();

      $rootScope.$on('$stateChangeStart', function (event, toState) {
        const search = new URL($location.absUrl()).search;

        $window.scrollTo(0, 0);

        if (search && AuthenticationService.isAuthenticated()) {
          const token = search.split('code=')[1];

          if (token) {
            AuthenticationService.authMl(token)
              .then(response => {
                if (response.statusText === 'OK') {
                  const offerMP = localStorageService.get('offerMP');
                  const enableMP = localStorageService.get('enableMP');
                  const productMP = localStorageService.get('productMP');

                  if (offerMP) {
                    localStorageService.remove('offerMP');
                    $state.go('quotationsAddOffer', {
                      custom: offerMP.custom ? 'custom' : 'generic',
                      quotationId: offerMP.id
                    });
                  } else if (productMP) {
                    $state.go('productCreate');
                  } else if (enableMP) {
                    $state.go('profile');
                  }
                }
              })
              .catch(() => undefined);
          }
        }


        var authorizedRoles = toState.data.authorizedRoles;
        var requireLogin = toState.data.requireLogin;

        if (!AuthenticationService.isAuthorized(authorizedRoles, requireLogin)) {
          if (toState.url !== '/login') {
            event.preventDefault();
            $state.go('login');
          }
        }
     });
    }

})();
