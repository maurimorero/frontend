(function () {
'use strict';

  angular
    .module('mlaApp')
    .config(routes);

    routes.$inject = ['$stateProvider', '$urlRouterProvider', 'USER_ROLES'];

    function routes($stateProvider, $urlRouterProvider, USER_ROLES) {

      $urlRouterProvider.otherwise('/home');

      $stateProvider
        .state('home', {
          url: '/home',
          templateUrl: 'app/product/views/product.find.view.html',
          controller: 'ProductFindController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('mpSucces', {
          url: '/ml-success-purchase/{id}',
          templateUrl: 'app/purchases/views/purchase.state.view.html',
          controller: 'PurchaseStateController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            state: 'success',
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('mpFailure', {
          url: '/ml-failure-purchase/{id}',
          templateUrl: 'app/purchases/views/purchases.view.html',
          controller: 'PurchasesController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            state: 'failure',
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('mpSuccess', {
          url: '/ml-pending-purchase/{id}',
          templateUrl: 'app/purchases/views/purchases.view.html',
          controller: 'PurchasesController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            state: 'pending',
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('login', {
          url: '/login',
          templateUrl: 'app/login/views/login.view.html',
          controller: 'LoginController',
          controllerAs: 'loginVm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('forgotpass', {
          url: '/forgotpass',
          templateUrl: 'app/login/views/login.forgotpass.view.html',
          controller: 'ForgotPassController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('resetpass', {
          url: '/resetpass',
          templateUrl: 'app/login/views/login.reset-pass.view.html',
          controller: 'ResetPassController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('newPass', {
          url: '/reset/{code}',
          templateUrl: 'app/login/views/login.new-pass.view.html',
          controller: 'NewPassController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('signup', {
          url: '/signup',
          templateUrl: 'app/signup/views/signup.view.html',
          controller: 'SignupController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('activate', {
          url: '/activate/{userId}/{code}',
          templateUrl: 'app/signup/views/activate.view.html',
          controller: 'ActivateController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('profile', {
          url: '/profile',
          templateUrl: 'app/profile/views/profile.view.html',
          controller: 'ProfileController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotations', {
          url: '/quotations',
          templateUrl: 'app/quotations/views/quotations.view.html',
          controller: 'QuotationsController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            selfQuotations: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationsList', {
          url: '/quotations/list',
          templateUrl: 'app/quotations/views/quotations.view.html',
          controller: 'QuotationsController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            selfQuotations: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationsCreate', {
          url: '/quotations/create/{id}',
          templateUrl: 'app/quotations/views/quotations.create.view.html',
          controller: 'QuotationsCreateController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationDetail', {
          url: '/quotation/detail/{id}',
          templateUrl: 'app/quotations/views/quotations.detail.view.html',
          controller: 'QuotationsDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            custom: false,
            offer: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationDetailOffer', {
          url: '/offer/detail/{id}',
          templateUrl: 'app/quotations/views/quotations.detail.view.html',
          controller: 'QuotationsDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            custom: false,
            offer: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationCustomDetail', {
          url: '/custom-quotation/detail/{id}',
          templateUrl: 'app/quotations/views/quotations.detail.view.html',
          controller: 'QuotationsDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            custom: true,
            offer: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationCustomDetailOffer', {
          url: '/custom-offer/detail/{id}',
          templateUrl: 'app/quotations/views/quotations.detail.view.html',
          controller: 'QuotationsDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            offer: true,
            custom: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationsOffers', {
          url: '/quotation/offers/{quotationId}',
          templateUrl: 'app/quotations/views/quotations.offers.view.html',
          controller: 'QuotationsOffersController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            custom: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('customQuotationsOffers', {
          url: '/custom-quotation/offers/{quotationId}',
          templateUrl: 'app/quotations/views/quotations.offers.view.html',
          controller: 'QuotationsOffersController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            custom: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationsAddOffer', {
          url: '/quotations/{custom}/offer/add/{quotationId}',
          templateUrl: 'app/quotations/views/quotations.offers-add.view.html',
          controller: 'QuotationsOfferAddController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            update: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('quotationUpdateOffer', {
          url: '/quotation/offer/update/{quotationId}',
          templateUrl: 'app/quotations/views/quotations.offers-add.view.html',
          controller: 'QuotationsOfferAddController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            update: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('productCreate', {
          url: '/product/create',
          templateUrl: 'app/product/views/product.create.view.html',
          controller: 'ProductCreateController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('productFind', {
          url: '/products',
          templateUrl: 'app/product/views/product.find.view.html',
          controller: 'ProductFindController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            myProducts: false,
            favorites: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('myProducts', {
          url: '/myproducts',
          templateUrl: 'app/product/views/product.find.view.html',
          controller: 'ProductFindController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            myProducts: true,
            favorites: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('favoriteProducts', {
          url: '/favorites',
          templateUrl: 'app/product/views/product.find.view.html',
          controller: 'ProductFindController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            myProducts: false,
            favorites: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('myOffers', {
          url: '/myoffers',
          templateUrl: 'app/quotations/views/quotations.view.html',
          controller: 'QuotationsController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            myOffers: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('productBuy', {
          url: '/product/buy/{productId}/{sellerId}/{priceId}',
          templateUrl: 'app/product/views/product.buy.view.html',
          controller: 'ProductBuyController',
          controllerAs: 'vm',
          data: {
            type: 'product',
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('offerBuy', {
          url: '/offer/buy/{sellerId}/{offerId}',
          templateUrl: 'app/product/views/product.buy.view.html',
          controller: 'ProductBuyController',
          controllerAs: 'vm',
          data: {
            type: 'offer',
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('productDetail', {
          url: '/product/detail/{id}',
          templateUrl: 'app/product/views/product.detail.view.html',
          controller: 'ProductDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        // .state('productInfo', {
        //   url: '/product/info/{id}',
        //   templateUrl: 'app/product/views/product.info.view.html',
        //   controller: 'ProductInfoController',
        //   controllerAs: 'vm',
        //   data: {
        //     requireLogin: false,
        //     authorizedRoles: [USER_ROLES.all]
        //   }
        // })
        // .state('productPricesList', {
        //   url: '/product/prices/{id}',
        //   templateUrl: 'app/product/views/product.prices-list.view.html',
        //   controller: 'ProductPricesListController',
        //   controllerAs: 'vm',
        //   data: {
        //     requireLogin: false,
        //     myProducts: false,
        //     favorites: true,
        //     authorizedRoles: [USER_ROLES.all]
        //   }
        // })
        .state('productAddPrice', {
          url: '/product/price/add/{productId}',
          templateUrl: 'app/product/views/product.prices-add.view.html',
          controller: 'ProductPricesAddController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            update: false,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('productUpdatePrice', {
          url: '/product/price/update/{productId}',
          templateUrl: 'app/product/views/product.prices-add.view.html',
          controller: 'ProductPricesAddController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            update: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchases', {
          url: '/purchases',
          templateUrl: 'app/purchases/views/purchases.view.html',
          controller: 'PurchasesController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseDetail', {
          url: '/purchase/detail/{id}',
          templateUrl: 'app/purchases/views/purchases.detail.view.html',
          controller: 'PurchasesDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseRating', {
          url: '/purchase/rating/{purchaseId}',
          templateUrl: 'app/rating/views/rating.view.html',
          controller: 'RatingController',
          controllerAs: 'vm',
          data: {
            purchase: true,
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseComplaint', {
          url: '/purchase/complaint/{purchaseId}',
          templateUrl: 'app/purchases/views/purchases.complaint.view.html',
          controller: 'PurchasesComplaintController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseComplaintsList', {
          url: '/purchase/complaints',
          templateUrl: 'app/purchases/views/purchases.complaints-list.view.html',
          controller: 'ComplaintsController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseComplaintDetail', {
          url: '/purchase/complaint-detail/{complaintId}',
          templateUrl: 'app/purchases/views/purchases.complaint-detail.view.html',
          controller: 'PurchasesComplaintDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('purchaseSellerDetail', {
          url: '/purchase/seller-detail/{id}',
          templateUrl: 'app/purchases/views/purchases.seller-detail.view.html',
          controller: 'PurchasesSellerDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('sales', {
          url: '/sales',
          templateUrl: 'app/sales/views/sales.list.view.html',
          controller: 'SalesListController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('saleDetail', {
          url: '/sale/detail/{id}',
          templateUrl: 'app/sales/views/sales.detail.view.html',
          controller: 'SalesDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('saleRating', {
          url: '/sale/rating/{purchaseId}',
          templateUrl: 'app/rating/views/rating.view.html',
          controller: 'RatingController',
          controllerAs: 'vm',
          data: {
            sale: true,
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('saleBuyerDetail', {
          url: '/sale/buyer-detail/{id}',
          templateUrl: 'app/sales/views/sales.buyer-detail.view.html',
          controller: 'SaleBuyerDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('saleSentConfirmation', {
          url: '/sale/sent-confirmation/{purchaseId}',
          templateUrl: 'app/sales/views/sales.sent-confirmation.view.html',
          controller: 'SaleSentConfirmationController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('saleState', {
          url: '/sale/state/{id}',
          templateUrl: 'app/sales/views/sales.state.view.html',
          controller: 'SaleStateController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('questions', {
          url: '/questions',
          templateUrl: 'app/questions/views/questions.view.html',
          controller: 'QuestionsController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        })
        .state('questionDetail', {
          url: '/question/detail/{id}',
          templateUrl: 'app/questions/views/questions.detail.view.html',
          controller: 'QuestionsDetailController',
          controllerAs: 'vm',
          data: {
            requireLogin: true,
            authorizedRoles: [USER_ROLES.all]
          }
        });
    }
})();