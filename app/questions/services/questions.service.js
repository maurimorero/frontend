(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('QuestionsService', QuestionsService);

  QuestionsService.$inject = ['$http', '$q', 'API'];

  function QuestionsService($http, $q, API) {
    const service = {
      getQuestions,
      getQuestion
    };

    return service;

    ////////////////

    function getQuestions(query) {
      const deferred = $q.defer();

      $http.get(API.getQuestions, {params: query || {}})
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function getQuestion(id, query) {
      const deferred = $q.defer();

      $http.get(API.getQuestion(id), {params: query || {}})
        .then(response => deferred.resolve(response.data.objects));

      return deferred.promise;
    }
  }
})();