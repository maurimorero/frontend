(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuestionsController', QuestionsController);

  QuestionsController.inject = ['$q', 'QuestionsService', 'QuotationsService', 'SessionService'];
  function QuestionsController($q, QuestionsService, QuotationsService, SessionService) {
    const vm = this;

    vm.userRole = SessionService.getUserRole();
    vm.questions = [];
    vm.sort = sort;
    vm.state = state;

    vm.pagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 20
    };

    vm.questionsState = {
      state: '-date_created',
      sort: ''
    };

    init();

    ////////////////

    function questionsSeller(query) {
      const userId = SessionService.getUserId();

      QuestionsService.getQuestion(userId, query)
        .then(questions => {
          vm.questions = questions;
          vm.pagination.totalItems = _.size(questions);

          questions.forEach(question => {
            QuotationsService.getQuotation(question.estimation_id)
              .then(estimation => {
                question.thumb_url_1 = estimation.thumb_url_1;
                question.product = estimation.product;
              });
          });

          questions.reverse();
          vm.questionsAll = questions;
        });
    }

    function questionsBuyer(query) {
      QuestionsService.getQuestions(query).then(questions => {
        vm.questions = questions;
        vm.pagination.totalItems = _.size(questions);

        questions.forEach(question => {
          QuotationsService.getQuotation(question.estimation)
            .then(estimation => {
              question.thumb_url_1 = estimation.thumb_url_1;
              question.product = estimation.product;
            });
        });

        questions.reverse();
        vm.questionsAll = questions;
      });
    }

    function init() {
      if (vm.userRole === 'seller') {
        questionsSeller();
      } else if (vm.userRole === 'buyer') {
        questionsBuyer();
      }
    }

    function state() {
      if (vm.userRole === 'seller') {
        const query = {
          order_by: vm.questionsState.state
        };
        questionsSeller(query);
      } else if (vm.userRole === 'buyer') {
        const query = {
          ordering: vm.questionsState.state
        };
        questionsBuyer(query);
      }
    }

    function sort() {
      if (vm.questionsState.sort === 'answered') {
        vm.questions = vm.questionsAll.filter(q => _.size(q.replies));
      } else if (vm.questionsState.sort === 'not_answered') {
        vm.questions = vm.questionsAll.filter(q => !_.size(q.replies));
      } else {
        vm.questions = vm.questionsAll;
      }
    }
  }
})();
