(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuestionsDetailController', QuestionsDetailController);

  QuestionsDetailController.inject = ['$q', '$stateParams', 'SessionService'];
  function QuestionsDetailController($q, $stateParams, SessionService) {
    const vm = this;

    vm.quotationId = $stateParams.id;
    vm.userRole = SessionService.getUserRole();

    init();

    ////////////////

    function init() {}
  }
})();
