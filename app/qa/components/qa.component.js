(function () {
'use strict';

  // Usage: <qa answer="bool" item-id="int"></qa>
  //
  // Creates: questions and answers component
  //

  angular
    .module('mlaApp')
    .component('qa', {
      templateUrl: 'app/qa/views/qa.component.view.html',
      controller: QAController,
      bindings: {
        answer: '<', // enables or disables the answer section
        itemId: '<', // id of the item to search for the questions
        custom: '<',
        complaint: '<'
      }
    });

  QAController.$inject = ['QaService', 'SessionService', 'PurchasesService', '$window'];

  function QAController(QaService, SessionService, PurchasesService, $window) {
    const ctrl = this;

    ctrl.$onInit = onInit;
    ctrl.$onChanges = onChanges;
    ctrl.askQuestion = askQuestion;
    ctrl.answerQuestion = answerQuestion;
    ctrl.userRole = SessionService.getUserRole();
    ctrl.answer = ctrl.answer || false;

    ctrl.questionAnswer = {};

    ctrl.cancel = cancel;

    ////////////////

    function onInit() {}

    function onChanges(changesObj) {
      if (changesObj.itemId.currentValue) {
        let get;

        if (ctrl.complaint) {
          get = PurchasesService.getComplaintQuestions;
        } else {
          get = ctrl.custom ? QaService.getCustomQuestions : QaService.getQuestions;
        }

        get(ctrl.itemId).then(questions => {
          _.reverse(ctrl.questions = questions);
        });
      }
    }

    function askQuestion() {
      // TODO: change estimation_id field to item_id
      const data = {
        user_id: SessionService.getUserId(),
        question: ctrl.question
      };

      if (!ctrl.complaint) {
        if (ctrl.custom) {
          data.custom_estimation_id = ctrl.itemId;
        } else {
          data.estimation_id = ctrl.itemId;
        }
      } else {
        data.complaint_id = ctrl.itemId;
      }

      const post = ctrl.complaint ? PurchasesService.createComplaintQuestion : QaService.createQuestion;

      post(data).then(() => {
        ctrl.questions = _.concat({question: ctrl.question, date_created: moment().format('D-MM-Y')}, ctrl.questions);
        ctrl.question = '';
      });
    }

    function answerQuestion(questionId) {
      const data = {
        question_id: questionId,
        user_id: SessionService.getUserId(),
        answer: ctrl.questionAnswer[questionId]
      };

      const post = ctrl.complaint ? PurchasesService.createComplaintReply : QaService.createReply;

      post(data).then(() => {
        const quotation = _.find(ctrl.questions, {id: questionId});
        quotation && quotation.replies.push({
          answer: ctrl.questionAnswer[questionId],
          date_created: moment().format('D-MM-Y')
        });

        ctrl.questionAnswer[questionId] = '';
      });
    }

    function cancel(e) {
      e.preventDefault();
      $window.history.back();
    }
  }
})();
