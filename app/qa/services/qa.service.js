(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('QaService', QaService);

  QaService.$inject = ['$http', '$q', 'API'];

  function QaService($http, $q, API) {
    const service = {
      createQuestion,
      getQuestions,
      getCustomQuestions,
      createReply
    };

    return service;

    ////////////////

    function createQuestion(data) {
      return $http.post(API.createQuestion, data);
    }

    function createReply(data) {
      return $http.post(API.createReply, data);
    }

    function getQuestions(id) {
      const deferred = $q.defer();

      $http
        .get(API.getQuotationsQuestions(id))
        .then(response => {
          return deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }

    function getCustomQuestions(id) {
      const deferred = $q.defer();

      $http
        .get(API.getCustomQuotationsQuestions(id))
        .then(response => {
          return deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }
  }
})();