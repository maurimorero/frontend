(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('RatingController', PurchasesRatingController);

  PurchasesRatingController.inject = ['$stateParams', '$state', 'PurchasesService'];
  function PurchasesRatingController($stateParams, $state, PurchasesService) {
    const vm = this;

    vm.rate = 0;
    vm.max = 5;
    vm.isReadonly = false;
    vm.status = {
      error: '',
      success: false
    };

    vm.hoveringOver = value => {
      vm.overStar = value;
      vm.percent = 100 * (value / vm.max);
    };


    vm.sale = _.get($state, 'current.data.sale', false);
    vm.purchase = _.get($state, 'current.data.purchase', false);

    vm.setRating = setRating;

    init();

    ////////////////

    function init() {}

    function setRating() {
      const purchaseId = $stateParams.purchaseId;
      if (purchaseId) {
        vm.dataLoading = true;

        const setRating = vm.sale
          ? PurchasesService.setSellerRating
          : PurchasesService.setBuyerRating;

        setRating({
          calification: vm.rate,
          purchase_id: purchaseId,
          comments: vm.comments
        })
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';
        })
        .catch(err => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = err.data && err.data.error;
        });
      }
    }
  }
})();