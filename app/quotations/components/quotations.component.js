(function () {
'use strict';

  // Usage: <quotation></quotation>
  //
  // Creates: quotation component
  //

  angular
    .module('mlaApp')
    .component('quotation', {
      templateUrl: 'app/quotations/views/quotations.component.view.html',
      controller: QuotationComponentController,
      bindings: {
        quotation: '<',
        type: '@',
        userOffer: '<',
        view: '@'
      }
    });

  function QuotationComponentController() {
    const ctrl = this;

    ctrl.productImage = '';
    ////////////////

    ctrl.$onInit = function () {
      if (ctrl.quotation.thumb_url_1) {
        ctrl.productImage = ctrl.quotation.image_1;
      } else {
        ctrl.productImage = ctrl.quotation.default_image;
      }
    };
    // ctrl.$onChanges = function (changesObj) {};
    // ctrl.$onDestory = function() { };
  }
})();