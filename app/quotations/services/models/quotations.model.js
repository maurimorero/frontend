(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('QuotationModel', QuotationModel);

    QuotationModel.$inject = ['SessionService'];

    function QuotationModel(SessionService) {

      class Quotation {

        constructor(data) {
          this.custom = false;
          this.setData(data);
        }

        /**
         * Creates an instance of Quotation
         *
         * @param  {Object} data
         */
        static create(data) {
          return new Quotation(data);
        }

        /**
         * Calculates the days until a Quotation expires
         */
        get remainingDays() {
          return moment(this.date_end).diff(moment(), 'days') + 1;
        }

        /**
         * @param  {Object} data (json data from the server)
         */
        setData(data) {
          if (data) {
            if (data.date_end) {
              data.date_end = new Date(data.date_end);
            }
            angular.extend(this, data);
          }
        }

        /**
         * Sets the data information inside product
         *
         * @param  {Object} data
         */
        setProduct(data) {
          this.setData({product: data});
        }

        setCustom(custom) {
          this.custom = custom;
          return this;
        }

        /**
         * Exports the data that'll be sent to the server
         */
        exportData() {
          let quotation = this;
          let data = {
            date_end: moment(quotation.date_end).format('YYYY-MM-DD'),
            count: quotation.count,
            comments: quotation.comments,
            buyer_id: SessionService.getUserId()
          };

          if (this.custom) {
            data.requested_product = quotation.requested_product;
            data.category_id = quotation.category.id;
            data.sub_category_id = quotation.subCategory.id;
            // data.image_1 = quotation.image_1;
            // data.image_2 = quotation.image_2;
          } else {
            data.product_id = quotation.product.id;
          }

          return data;
        }
      }

      return Quotation;
    }
})();