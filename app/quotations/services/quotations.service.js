(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('QuotationsService', QuotationsService);

  QuotationsService.$inject = ['$http', '$q', 'API', 'QuotationModel'];

  function QuotationsService($http, $q, API, QuotationModel) {
    const service = {
      getQuotations,
      getQuotation,
      getCustomQuotations,
      getCustomQuotation,
      create,
      createCustom,
      createQuestion,
      getQuestions,
      createReply,
      addOffer,
      addCustomOffer,
      getMyOffers,
      cancelOffer,
      getQuotationAttributes,
      setQuotationAttributes
    };

    return service;

    ////////////////

    function getQuotations(query) {
      const deferred = $q.defer();

      $http.get(API.getQuotations, {params: query || {}})
        .then(response => {
          deferred.resolve(response.data.objects.map(QuotationModel.create));
        });

      return deferred.promise;
    }

    function getCustomQuotations(query) {
      const deferred = $q.defer();

      $http.get(API.getCustomQuotations, {params: query || {}})
        .then(response => {
          const quotations = response.data.objects.map(q => {
            return QuotationModel.create(q).setCustom(true);
          });

          deferred.resolve(quotations);
        });

      return deferred.promise;
    }

    function getQuotation(id) {
      const deferred = $q.defer();

      $http.get(API.getQuotation(id))
        .then(response => {
          deferred.resolve(QuotationModel.create(response.data));
        });

      return deferred.promise;
    }

    function getCustomQuotation(id) {
      const deferred = $q.defer();

      $http.get(API.getCustomQuotation(id))
        .then(response => {
          deferred.resolve(QuotationModel.create(response.data));
        });

      return deferred.promise;
    }

    function create(data) {
      const deferred = $q.defer();

      $http.post(API.createQuotation, data)
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function createCustom(data) {
      const deferred = $q.defer();

      $http.post(API.createCustomQuotation, data)
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function createQuestion(data) {
      return $http.post(API.createQuestion, data);
    }

    function createReply(data) {
      return $http.post(API.createReply, data);
    }

    function getQuestions(id) {
      const deferred = $q.defer();

      $http.get(API.getQuotationsQuestions(id))
        .then(response => {
          return deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }

    function addOffer(data) {
      return $http.post(API.addQuotationOffer, data);
    }

    function addCustomOffer(data) {
      return $http.post(API.addCustomQuotationOffer, data);
    }

    function getMyOffers(query) {
      const deferred = $q.defer();

      $http.get(API.myOffers, {params: query || {}})
        .then(response => {
          return deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }

    function cancelOffer(id) {
      const deferred = $q.defer();

      $http.get(API.cancelOffer(id))
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function getQuotationAttributes(id) {
      const deferred = $q.defer();

      $http.get(API.getQuotationAttributes(id))
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function setQuotationAttributes(id, data) {
      return $http.post(API.setQuotationAttributes(id), data);
    }
  }

})();