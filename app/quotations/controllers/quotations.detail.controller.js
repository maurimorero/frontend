(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuotationsDetailController', QuotationsDetailController)
    .controller('ModalCtrl', function ($uibModalInstance, title) {
      const $ctrl = this;

      $ctrl.title = title;

      $ctrl.ok = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    });

  QuotationsDetailController.$inject = [
    '$stateParams',
    'QuotationsService',
    'ProductService',
    'SessionService',
    'UserService',
    'localStorageService',
    '$state',
    '$uibModal',
    '$document',
    '$window',
    '$location'
  ];

  function QuotationsDetailController($stateParams,
    QuotationsService,
    ProductService,
    SessionService,
    UserService,
    localStorageService,
    $state,
    $uibModal,
    $document,
    $window,
    $location
  ) {
    const vm = this;

    vm.customQuotation = _.get($state, 'current.data.custom', false);
    vm.isOffer = _.get($state, 'current.data.offer', false);
    vm.userRole = SessionService.getUserRole();
    vm.createOffer = createOffer;
    vm.cancelOffer = cancelOffer;
    vm.offerBuy = offerBuy;
    vm.open = open;
    vm.currentURL = $location.absUrl();

    vm.print = function () {
      $window.print();
    };

    init();

    /////////////////

    function init() {
      const quotationId = $stateParams.id;
      const userId = SessionService.getUserId();

      if (quotationId) {
        const getQuotation = vm.customQuotation
          ? QuotationsService.getCustomQuotation
          : QuotationsService.getQuotation;

        getQuotation(quotationId)
          .then(quotation => {
            vm.quotation = quotation;
            vm.quotationLoadedByUser = userId === quotation.buyer_id;
            vm.showQa = showQa();

            vm.offer = _.find(quotation.offers, {seller_id: userId});
            vm.offerLoadedByUser = !_.isUndefined(vm.offer);

            if (vm.customQuotation) {
              const byGroup = _.groupBy(quotation.attributes, 'group');
              vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));
            } else {
              return ProductService.getProduct(quotation.product_id);
            }
          })
          .then(product => {
            vm.product = product;

            if (product) {
              const byGroup = _.groupBy(product.attributes, 'group');
              vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));
            }
          });
      }
    }

    function showQa() {
      if (vm.quotation.is_active && vm.quotationLoadedByUser && vm.userRole === 'buyer') {
        return {
          showQa: true,
          answers: true
        };
      } else if (vm.quotation.is_active && !vm.quotationLoadedByUser && vm.userRole === 'seller') {
        return {
          showQa: true,
          answers: false
        };
      } else {
        return {
          showQa: false
        };
      }
    }

    function validateMP(data) {
      if (data.is_authorized_by_ml) {
        const offerMP = localStorageService.remove('offerMP');
        if (offerMP) {
          const search = new URL($location.absUrl());
          $window.location.replace(`${search.origin}${search.pathname}`);
        }
        return true;
      } else {
        open(null, 'Debe tener Mercado Pago activo antes de hacer una oferta')
          .then(function () {
            localStorageService.add('offerMP', {
              id: $stateParams.quotationId,
              custom: vm.isCustom
            });
            $state.go('profile');
          }, function () {});

        return false;
      }
    }

    function validateFacturationData(data) {
      let dataToCheck = [
        'tax_type_facturation',
        'street_facturation',
        'street_number_facturation',
        'city_facturation'
      ];

      if (data.tax_type_facturation !== 'F') {
        dataToCheck.push('business_name_facturation');
        dataToCheck.push('cuit_facturation');
      }

      const billingInfo = _.pick(data, dataToCheck);

      const isNotAvailable = _.some(billingInfo, info => _.isNull(info) || (_.isString(info) && _.isEmpty(info)));

      if (isNotAvailable) {
        localStorageService.set('offerCreate', {
          custom: vm.customQuotation,
          quotationId: vm.quotation.id
        });

        open(null, 'Debe completar los datos de facturación antes de poder realizar una oferta')
          .then(function () {
            $state.go('profile');
          }, function () {});

        return false;
      }
      return true;
    }

    function createOffer() {
      UserService.getUser().then(data => {
        if (validateFacturationData(data)) {
          if (validateMP(data)) {
            $state.go('quotationsAddOffer', {
              custom: vm.customQuotation ? 'custom' : 'generic',
              quotationId: vm.quotation.id
            });
          }
        }
      });
    }

    function cancelOffer() {
      open(null, 'Esta seguro de eliminar la oferta?')
        .then(function () {
          QuotationsService.cancelOffer(vm.offer.id)
            .then(() => $window.location.reload());
        }, function () {});
    }

    function open(parentSelector, title) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContent.html',
        controller: 'ModalCtrl',
        controllerAs: 'md',
        appendTo: parentElem,
        resolve: {
          title: function () {
            return title;
          }
        }
      });

      return modalInstance.result;
    }

    function validateDeliveryData() {
      return UserService.getUser().then(data => {
        const dataToCheck = [
          'city',
          'area_code',
          'street',
          'street_number'
        ];

        const deliveryInfo = _.pick(data, dataToCheck);

        const isNotAvailable = _.some(deliveryInfo, info => _.isNull(info) || (_.isString(info) && _.isEmpty(info)));

        if (isNotAvailable) {
          open(null, 'Debe completar los datos de entrega para realizar una Compra')
          .then(function () {
            localStorageService.set('offerBuy', true);
            $state.go('profile');
          }, function () {});
        }

        return isNotAvailable;
      });
    }

    function offerBuy(offerData) {
      validateDeliveryData().then(isNotValid => {
        if (!isNotValid) {
          $state.go('offerBuy', offerData);
        }
      });
    }

  }
})();
