(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuotationsController', QuotationsController);

  QuotationsController.$inject = ['$state', 'QuotationsService', 'ProductService', 'SessionService', 'PAGINATION'];

  function QuotationsController($state, QuotationsService, ProductService, SessionService, PAGINATION) {
    const vm = this;

    vm.quotationsList = [];
    vm.quotationsCustomList = [];
    vm.quotation = {
      order_by: 'date_created'
    };

    vm.offerSort = {};

    vm.userRole = SessionService.getUserRole();
    vm.selfQuotations = _.get($state, 'current.data.selfQuotations', false);
    vm.showMyOffers = _.get($state, 'current.data.myOffers', false);
    const userId = SessionService.getUserId();
    vm.search = search;
    vm.searchMyOffers = searchMyOffers;
    vm.getOfferType = getOfferType;
    vm.grid = true;

    vm.setGridView = function (view) {
      vm.grid = view;
    };

    vm.offerLoadedByUser = offerLoadedByUser;

    // Pagination settings
    vm.pagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    vm.pQ = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    vm.pCQ = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    init();

    ////////////////

    function init() {
      getAttributes();
      ProductService.getCategories()
        .then(categories => vm.categories = categories);

      SessionService.userLoggedIn() && ProductService.getProducts()
        .then(products => {
          vm.productsList = products;
          vm.pagination.totalItems = _.size(products);
        });

      if (vm.showMyOffers) {
        searchMyOffers();
      } else {
        search();
      }
    }

    function search(keyEvent) {
      if (keyEvent && keyEvent.which !== 13) return;

      vm.dataLoading = true;

      let query = {
        'product-name': _.get(vm.quotation, 'product'),
        orderBy: _.get(vm.quotation, 'orderBy'),
        sub_category_id: _.get(vm.quotation, 'subCategory'),
        attrs_id: _.get(vm.quotation, 'attr'),
        order_by: _.get(vm.quotation, 'order_by'),
        active: 'True'
      };

      const state = _.get(vm.quotation, 'state');

      if (state) {
        const filter = state.split('=')[0];
        const value = state.split('=')[1];

        query[filter] = value;
      }

      if (vm.selfQuotations) {
        query.buyer_id = SessionService.getUserId();
      }

      QuotationsService.getQuotations(query)
        .then(quotations => {
          // vm.dataLoading = false;
          vm.pQ.totalItems = _.size(quotations);
          addQuotationsPrice(quotations);
          vm.quotationsList = quotations;

          return QuotationsService.getCustomQuotations(query);
        })
        .then(quotations => {
          vm.dataLoading = false;
          vm.pQ.totalItems += _.size(quotations);
          addQuotationsPrice(quotations);

          vm.quotationsList = vm.quotationsList.concat(quotations);
          // console.log('=>', vm.quotationsList);
        });
    }

    function searchMyOffers() {
      let query = {
        category: _.get(vm.offerSort, 'category.id'),
        order_by: _.get(vm.offerSort, 'orderBy')
      };

      const state = _.get(vm.offerSort, 'state');

      if (state) {
        const filter = state.split('=')[0];
        const value = state.split('=')[1];

        query[filter] = value;
      }

      QuotationsService.getMyOffers(query)
        .then(quotations => {
          vm.dataLoading = false;
          vm.offers = quotations;
        });
    }

    function getOfferType(quotation) {
      return _.isEmpty(quotation.custom_estimation) ? 'generic' : 'custom';
    }

    function offerLoadedByUser(quotation) {
      vm.offer = _.find(quotation.offers, {seller_id: userId});
      return !_.isUndefined(vm.offer) || vm.showMyOffers;
    }

    function addQuotationsPrice(quotations) {
      quotations.forEach(quotation => {
        const offer = _.find(quotation.offers, {seller_id: userId});
        if (!_.isUndefined(offer)) {
          quotation.price = offer.price;
        } else {
          const minOffer = _.minBy(quotation.offers, offer => parseFloat(offer.price));
          quotation.price = _.get(minOffer, 'price', '-');
        }
      });

    }

    function getAttributes() {
      ProductService.getAttributes()
        .then(attributes => {
          vm.attributes = _.groupBy(_.uniqWith(attributes, _.isEqual), 'feature');
        });
    }

  }
})();