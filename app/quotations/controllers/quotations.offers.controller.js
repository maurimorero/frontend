(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuotationsOffersController', QuotationsOffersController);

  QuotationsOffersController.$inject = ['$state', '$stateParams', 'QuotationsService'];

  function QuotationsOffersController($state, $stateParams, QuotationsService) {
    const vm = this;

    vm.offers = [];
    vm.customQuotation = _.get($state, 'current.data.custom', false);

    const quotationId = $stateParams.quotationId;

    vm.pagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 20
    };

    init();

    ////////////////

    function init() {
      const getQuotation = vm.customQuotation
        ? QuotationsService.getCustomQuotation
        : QuotationsService.getQuotation;

      getQuotation(quotationId)
        .then(quotation => {
          vm.pagination.totalItems = _.size(quotation.offers);
          vm.offers = quotation.offers;
        });
    }
  }
})();