(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuotationsOfferAddController', QuotationsOfferAddController);

  QuotationsOfferAddController.inject = [
    '$state',
    '$stateParams',
    '$uibModal',
    '$document',
    '$window',
    '$location',
    'QuotationsService',
    'SessionService',
    'UserService',
    'localStorageService'
  ];

  function QuotationsOfferAddController(
    $state,
    $stateParams,
    $uibModal,
    $document,
    $window,
    $location,
    QuotationsService,
    SessionService) {

    const vm = this;

    vm.updateOffer = _.get($state, 'current.data.update', false);
    vm.isCustom = $stateParams.custom === 'custom';
    vm.quotationId = $stateParams.quotationId;

    vm.status = {
      error: '',
      success: false
    };

    vm.addOffer = addOffer;

    ////////////////

    function addOffer() {
      vm.dataLoading = true;

      const data = {
        seller_id: SessionService.getUserId(),
        estimation_id: vm.quotationId,
        price: vm.price,
        description: (vm.description || '').trim()
      };

      const addOffer = vm.isCustom
        ? QuotationsService.addCustomOffer
        : QuotationsService.addOffer;

      addOffer(data)
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';
        })
        .catch(err => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = err.data && err.data.error;
        });
    }
  }
})();