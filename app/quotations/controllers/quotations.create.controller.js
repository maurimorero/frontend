(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('QuotationsCreateController', QuotationsCreateController)
    .controller('QuotationModalCtrl', function ($uibModalInstance) {
      const $ctrl = this;

      $ctrl.ok = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    });

  QuotationsCreateController.$inject = ['$stateParams',
    '$state',
    '$uibModal',
    '$document',
    'QuotationModel',
    'ProductService',
    'QuotationsService',
    'SessionService',
    'localStorageService',
    'AuthenticationService',
    'UserService'
  ];

  function QuotationsCreateController(
    $stateParams,
    $state,
    $uibModal,
    $document,
    QuotationModel,
    ProductService,
    QuotationsService,
    SessionService,
    localStorageService,
    AuthenticationService,
    UserService
  ) {
    const vm = this;
    const productId = $stateParams.id;

    vm.step = 0;

    vm.dataLoading = false;
    vm.status = {
      error: '',
      success: false
    };
    vm.quotation = QuotationModel.create();
    vm.genericQuotation = $stateParams.id ? false : true;
    vm.quotation.setCustom(vm.genericQuotation);
    vm.updateSubCategory = updateSubCategory;
    vm.next = next;
    vm.save = save;
    vm.attributeSelected = {};
    vm.saveCustom = saveCustom;

    // angular ui calendar configuration
    vm.calendar = {
      openedEnd: false,
      format: 'dd MMMM yyyy',
      dateOptions: {
        maxDate: new Date(2020, 5, 22),
        minDate: moment().toDate(),
        startingDay: 1
      },
      openEnd() {
        this.openedEnd = true;
      }
    };

    vm.locations = [
      'Buenos Aires',
      'Catamarca',
      'Chaco',
      'Chubut',
      'Córdoba',
      'Corrientes',
      'Entre Ríos',
      'Formosa',
      'Jujuy',
      'La Pampa',
      'La Rioja',
      'Mendoza',
      'Misiones',
      'Neuquén',
      'Río Negro',
      'Salta',
      'San Juan',
      'San Luis',
      'Santa Cruz',
      'Santa Fe',
      'Santiago del Estero',
      'Tierra del Fuego',
      'Tucumán'
    ];

    init();

    ////////////////

    function init() {
      const userLoggedIn = SessionService.userLoggedIn();
      const savedData = localStorageService.get('quotation');

      const quotationCreateContinuation = localStorageService.get('quotationCreate');

      if (vm.genericQuotation && savedData && !userLoggedIn) {
        vm.quotation.setData(_.omit(savedData, ['attrs', 'comments']));
        vm.comments = savedData.comments;
        vm.attributeSelected = savedData.attrs;
        updateSubCategory();
      } else if (vm.genericQuotation && savedData && userLoggedIn) {
        vm.quotation.setData(_.omit(savedData, ['attrs', 'comments']));
        vm.comments = savedData.comments;
        vm.attributeSelected = savedData.attrs;
        saveCustom();
      } else if (!vm.genericQuotation && savedData && userLoggedIn) {
        vm.quotation.setData(savedData);
        vm.product = vm.quotation.product;
        save();
      } else if (!vm.genericQuotation && savedData && !userLoggedIn) {
        vm.quotation.setData(savedData);
        vm.product = vm.quotation.product;
      } else if (vm.genericQuotation && quotationCreateContinuation) {
        vm.quotation.setData(_.omit(quotationCreateContinuation, ['attrs', 'comments']));
        vm.comments = quotationCreateContinuation.comments;
        vm.attributeSelected = quotationCreateContinuation.attrs;
        saveCustom();
      } else if (!vm.genericQuotation && quotationCreateContinuation) {
        vm.quotation.setData(quotationCreateContinuation);
        vm.product = vm.quotation.product;
      } else {
        if (productId) {
          ProductService.getProduct(productId)
            .then(product => {
              vm.product = product;
              vm.quotation.setProduct(product);
            });
        } else {
          ProductService.getCategories()
            .then(categories => vm.categories = categories);
        }
      }

      localStorageService.remove('quotationCreate');
    }

    function save() {
      const userLoggedIn = SessionService.userLoggedIn();

      if (userLoggedIn) {
        vm.dataLoading = true;

        validateDeliveryData().then(isNotValid => {
          vm.dataLoading = true;

          if (!isNotValid) {
            QuotationsService.create(vm.quotation.exportData())
              .then(() => {
                vm.dataLoading = false;
                vm.status.success = true;
                vm.status.error = '';
                localStorageService.remove('quotation');
              })
              .catch(() => {
                vm.dataLoading = false;
                vm.status.success = false;
                vm.status.error = 'Error al guardar los datos';
              });
          }
        });
      } else {
        localStorageService.set('quotation', _.assign({}, vm.quotation));
        AuthenticationService.setPrevState({
          state: 'quotationsCreate',
          params: {id: productId}
        });
        $state.go('login');
      }
    }

    function next() {
      vm.step = 1;
      vm.dataLoading = true;

      const savedData = localStorageService.get('quotation');

      QuotationsService.getQuotationAttributes(vm.quotation.subCategory.id)
        .then(attributes => {
          vm.dataLoading = false;

          const byGroup = _.groupBy(attributes, 'group');
          vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));

          if (savedData) {
            _.each(savedData.attrs, attr => vm.attribute[attr.attribute_id] = attr.value);
          }
        })
        .catch(() => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = 'Error al obtener los parametros';
        });
    }

    function mapAttributes(attributes) {
      return _.map(attributes, v => ({
        value: true, attribute_id: parseInt(v)
      }));
    }

    function validateDeliveryData() {
      return UserService.getUser().then(data => {
        const dataToCheck = [
          'city',
          'area_code',
          'street',
          'street_number'
        ];

        const deliveryInfo = _.pick(data, dataToCheck);

        const isNotAvailable = _.some(deliveryInfo, info => _.isNull(info) || (_.isString(info) && _.isEmpty(info)));

        if (isNotAvailable) {
          localStorageService.remove('quotation');

          if (vm.genericQuotation) {
            localStorageService.set('quotationCreate', _.assign({},
              vm.quotation,
              {
                attrs: mapAttributes(vm.attribute),
                comments: vm.comments
              }
            ));
          } else {
            localStorageService.set('quotationCreate', _.assign({}, vm.quotation));
          }

          open();
        }

        return isNotAvailable;
      });
    }

    function saveCustom() {
      if (SessionService.userLoggedIn()) {

        const data = {
          attrs: mapAttributes(vm.attributeSelected),
          comments: vm.comments
        };

        validateDeliveryData().then(isNotValid => {
          vm.dataLoading = true;

          if (!isNotValid) {
            QuotationsService.createCustom(
              angular.toJson(_.assign({}, vm.quotation.exportData(), data))
            )
            .then(() => {
              vm.dataLoading = false;
              vm.status.success = true;
              vm.status.error = '';
              localStorageService.remove('quotation');
            })
            .catch(() => {
              vm.dataLoading = false;
              vm.status.success = false;
              vm.status.error = 'Error al guardar los datos';
            });
          }
        });
      } else {
        localStorageService.set('quotation', _.assign({},
          vm.quotation,
          {
            attrs: vm.attributeSelected,
            comments: vm.comments
          }
        ));

        AuthenticationService.setPrevState({state: 'quotationsCreate'});
        $state.go('login');
      }
    }

    function updateSubCategory() {
      const query = {
        category_id: _.get(vm.quotation.category, 'id', '')
      };

      ProductService.getSubCategories(query)
        .then(subCategories => vm.subCategories = subCategories);
    }

    function open(parentSelector) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContent.html',
        controller: 'QuotationModalCtrl',
        controllerAs: 'md',
        appendTo: parentElem
      });

      modalInstance.result.then(function () {
        $state.go('profile');
      }, function () {
        vm.dataLoading = false;
      });
    }

  }
})();
