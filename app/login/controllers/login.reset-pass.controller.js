(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ResetPassController', ResetPassController);

  ResetPassController.$inject = ['$state', 'AuthenticationService'];

  function ResetPassController($state, AuthenticationService) {
    const vm = this;

    vm.change = change;
    vm.status = {
      success: false,
      error: ''
    };

    init();

    ////////////////

    function init() {
      vm.dataLoading = false;
    }

    function change() {
      vm.dataLoading = true;
      if (vm.password === vm.password2) {
        AuthenticationService.changePassword(vm.password)
          .then(() => {
            vm.dataLoading = false;
            vm.status.success = true;
            // setTimeout(() => $state.go('login'), 3000);
          })
          .catch(err => {
            vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = err.data && err.data.error;
          });
      }
    }
  }
})();