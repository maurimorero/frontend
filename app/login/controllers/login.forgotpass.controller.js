(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ForgotPassController', ForgotPassController);

  ForgotPassController.$inject = ['$state', 'AuthenticationService'];

  function ForgotPassController($state, AuthenticationService) {
    const vm = this;

    vm.change = change;
    vm.status = {
      success: false,
      error: ''
    };

    init();

    ////////////////

    function init() {
      vm.dataLoading = false;
    }

    function change() {
      vm.dataLoading = true;

      if (vm.email && vm.email.length && vm.email.trim()) {
        AuthenticationService.resetPassword(vm.email.trim())
          .then(() => {
            vm.dataLoading = false;
            vm.status.success = true;
          })
          .catch(err => {
            vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = err.data && err.data.error;
          });
      }
    }
  }
})();