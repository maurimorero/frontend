(function () {
  'use strict';

  angular
    .module('mlaApp')
    .controller('LoginController', LoginController);

    LoginController.$inject = ['$state', 'AuthenticationService'];

    function LoginController($state, AuthenticationService) {
      let vm = this;

      vm.login = login;

      init();

      ///////////////////////

      function init() {
        vm.dataLoading = false;
      }

      function login() {
        vm.loginFailed = false;
        vm.dataLoading = true;
        const prevStates = ['quotationsCreate'];

        AuthenticationService.login(vm.credentials)
          .then(() => {
            const prevState = AuthenticationService.getPrevState();

            if (!_.isUndefined(prevState) && prevStates.includes(prevState.state)) {
              if (_.size(prevState.params)) {
                $state.go(prevState.state, prevState.params);
              } else {
                $state.go(prevState.state);
              }
            } else {
              $state.go('home');
            }
          })
          .catch(() => {
            vm.loginFailed = true;
            vm.dataLoading = false;
            vm.responseErrorText = 'Usuario o contraseña incorrectos';
          });
      }
    }
})();