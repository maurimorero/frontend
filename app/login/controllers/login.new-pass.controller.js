(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('NewPassController', NewPassController);

  NewPassController.$inject = ['$stateParams', 'AuthenticationService'];

  function NewPassController($stateParams, AuthenticationService) {
    const vm = this;

    vm.change = change;
    vm.status = {
      success: false,
      error: ''
    };

    init();

    ////////////////

    function init() {
      vm.dataLoading = false;
    }

    function change() {
      vm.dataLoading = true;
      const code = _.get($stateParams, 'code');

      if (vm.password === vm.password2) {
        AuthenticationService.newPassword(code, vm.password)
          .then(() => {
            vm.dataLoading = false;
            vm.status.success = true;
          })
          .catch(err => {
            vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = err.data && err.data.error;
          });
      }
    }
  }
})();