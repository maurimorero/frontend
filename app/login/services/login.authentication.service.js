(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$q', '$rootScope', '$http', '$cookies', 'SessionService', 'API', 'AUTH_EVENTS'];

    function AuthenticationService($q, $rootScope, $http, $cookies, SessionService, API, AUTH_EVENTS) {
      let service = {
        login,
        logout,
        setCredentials,
        restoreCredentials,
        isAuthenticated,
        isAuthorized,
        setPrevState,
        getPrevState,
        activate,
        changePassword,
        resetPassword,
        newPassword,
        cleanSession,
        authMl
      };

      ///////////////////////////////

      /**
       * @param  {Object} credentials {username, password}
       */
      function login(credentials) {
        let deferred = $q.defer();

        $http.post(API.login, credentials)
          .then(response => {
            setCredentials(response.data);
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            deferred.resolve();
          })
          .catch(msg => {
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
            deferred.reject(msg);
          });

        return deferred.promise;
      }

      function cleanSession() {
        SessionService.destroy();
        $cookies.remove('csrftoken');
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
      }

      /**
       * Performs user logout
       */
      function logout() {
        let deferred = $q.defer();

        $http.get(API.logout)
          .then(() => {
            cleanSession();
            deferred.resolve();
          })
          .catch(msg => {
            deferred.reject(msg);
          });

          return deferred.promise;
      }

      /**
       * Sets the user credentials
       *
       * @param  {Object} user
       */
      function setCredentials(user) {
        SessionService.create({
          username: user.username,
          userId: user.id,
          userRole: 'buyer'
        });
      }

      /**
       * Retrieves the seesion data stored
       */
      function restoreCredentials() {
        SessionService.restore();
      }

      /**
       * Returns true if the user id is defined
       */
      function isAuthenticated() {
        return !!SessionService.getUserId();
      }

      /**
       * Returns true if the * is inside the autorized roles
       *
       * @param  {Array} authorizedRoles (list of roles defined in routes.js)
       */
      function isPublic(authorizedRoles) {
        return authorizedRoles.indexOf('*') !== -1;
      }

      /**
       * Checks if the user has the correct role (DEPRECATED)
       *
       * @param  {Array} authorizedRoles (list of roles defined in routes.js)
       */
      // function isRolePermitted(authorizedRoles) {
      //   return authorizedRoles.indexOf(SessionService.getUserRole()) !== -1;
      // }

      /**
       * A user is authorizes if is looking a page logged in and has
       * the correct role.
       *
       * @param  {Array} authorizedRoles (list of roles defined in routes.js)
       */
      function isAuthorized(authorizedRoles, requireLogin) {
        return requireLogin
                ? isPublic(authorizedRoles) && isAuthenticated()
                : isPublic(authorizedRoles);
      }

      function setPrevState(state) {
        this.prevState = {
          state: null,
          params: null
        };

        this.prevState.state = state.state;
        this.prevState.params = state.params;
      }

      function getPrevState() {
        return this.prevState;
      }

      function activate(userId, code) {
        return $http.post(API.activate, {
          code: code,
          user_id: userId
        });
      }

      function changePassword(password) {
        return $http.post(API.changePassword, {
          password
        });
      }

      function resetPassword(email) {
        return $http.post(API.resetPassword, {
          email
        });
      }

      function newPassword(code, password) {
        return $http.post(API.newPassword, {
          code,
          password
        });
      }

      function authMl(token) {
        return $http.post(API.authMl, { token });
      }

      return service;
    }
})();