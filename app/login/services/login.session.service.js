(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('SessionService', SessionService);

  SessionService.$inject = ['localStorageService'];

  function SessionService(localStorageService) {
    let session = {};

    let service = {
      getUserId,
      getUsername,
      getUserRole,
      setUserRole,
      userLoggedIn,
      restore,
      create,
      destroy
    };

    return service;

    ////////////////

    /**
     * Gets the logged in user id
     */
    function getUserId() {
      return session.userId;
    }

    /**
     * Gets the user logged in
     */
    function getUsername() {
      return session.username;
    }

    /**
     * Gets the role of the logged user
     */
    function getUserRole() {
      return session.userRole || 'buyer';
    }

    /**
     * Sets the role of the logged user
     */
    function setUserRole(role) {
      session.userRole = role;
      localStorageService.set('session', session);
    }

    /**
     * Returns true if the user is logged in
     */
    function userLoggedIn() {
      return !!getUserId();
    }

    /**
     * Restores the saved session
     */
    function restore() {
      session = localStorageService.get('session') || {};
    }

    /**
     * Stores session data
     * @param  {Object} sessionData {sessionId, userId}
     */
    function create(sessionData) {
      angular.extend(session, sessionData);
      localStorageService.set('session', sessionData);
    }

    /**
     * Destroys the data session
     */
    function destroy() {
      session = {};
      localStorageService.remove('session');
    }
  }
})();
