(function () {
  'use strict';

  angular
    .module('mlaApp')
    .directive('pwCheck', pwCheck);

  function pwCheck() {

    /**
     * Usage:
     * <input type='password' id='pw1' name='pw1' ng-model='pw1' ng-required/>
     * <input type='password' id='pw2' name='pw2' ng-model='pw2' ng-required pw-check='pw1'/>
     */

    let directive = {
      require: 'ngModel',
      link: link,
      restrict: 'A'
    };

    return directive;

    function link(scope, elem, attrs, ctrl) {
      var $firstPassword = angular.element('#' + attrs.pwCheck);

      elem.add($firstPassword).on('keyup', function () {
        scope.$apply(function () {
          let v = elem.val() === $firstPassword.val();

          ctrl.$setValidity('pwmatch', v);
        });
      });
    }
  }

})();
