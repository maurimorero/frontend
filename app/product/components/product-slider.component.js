(function () {
  'use strict';

  // Usage:
  //
  // Creates:
  //

  angular
    .module('mlaApp')
    .component('prdslider', {
      templateUrl: 'app/product/views/product-slider.component.view.html',
      controller: PrdSliderController,
      bindings: {
        product: '<'
      }
    });

  function PrdSliderController() {
    const $ctrl = this;

    ////////////////

    $ctrl.currentImage = undefined;

    $ctrl.slickConfig = {
      enabled: true,
      draggable: false,
      adaptativeWidth: false,
      slidesToShow: 4,
      slideToScroll: 1,
      prevArrow: '<button type="button" class="arrow-prev"><i class="fa fa-arrow-left"></i></button>',
      nextArrow: '<button type="button" class="arrow-next"><i class="fa fa-arrow-right"></i></button>'
      // variableWidth: true
      // dots: true
      // adaptiveHeight: false
      // autoplay: true,
      // autoplaySpeed: 3000
    };

    $ctrl.setImage = function (img) {
      $ctrl.currentImage = img;
    };

  }
})();
