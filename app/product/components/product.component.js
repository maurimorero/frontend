(function () {
'use strict';

  // Usage: <product></product>
  //
  // Creates: product component
  //

  angular
    .module('mlaApp')
    .component('product', {
      templateUrl: 'app/product/views/product.component.view.html',
      controller: ProductController,
      bindings: {
        product: '<',
        view: '@'
      }
    });

  ProductController.$inject = ['$rootScope', '$state', 'SessionService', 'ProductService'];

  function ProductController($rootScope, $state, SessionService, ProductService) {
    const ctrl = this;
    const userId = SessionService.getUserId();

    ctrl.userRole = SessionService.getUserRole();
    ctrl.loggedIn = SessionService.userLoggedIn();
    ctrl.favorite = false;

    $rootScope.$on('buyer', function () {
      ctrl.userRole = SessionService.getUserRole();
    });

    ctrl.addFavorite = function (productId) {
      if (ctrl.loggedIn) {
        ProductService.setProductFavorite({product_id: productId})
          .then(() => ctrl.favorite = !ctrl.favorite);
      } else {
        $state.go('login');
      }
    };

    ctrl.$onInit = function () {
      // Images
      if (ctrl.product.thumb_url_1) {
        ctrl.productImage = ctrl.product.image_1;
      } else {
        ctrl.productImage = ctrl.product.default_image;
      }

      // Prices
      if (_.isArray(ctrl.product.prices)) {
        ctrl.isPriceLoadedByUser = _.some(ctrl.product.prices, {seller_id: userId});

        ctrl.lowestPrice = ctrl.product.lowest_price; //_.minBy(ctrl.product.prices, price => parseFloat(price.price));

        // if (ctrl.isPriceLoadedByUser) {
        //   const price = _.find(ctrl.product.prices, {seller_id: userId});
        //   ctrl.priceLoadedByUser = price.price;
        //   ctrl.stockLoadedByUser = price.stock;
        // }
      } else if (_.isString(ctrl.product.price) && !_.isEmpty(ctrl.product.price)) {
        ctrl.priceLoadedByUser = ctrl.product.price;
        ctrl.isPriceLoadedByUser = true;
        ctrl.stockLoadedByUser = ctrl.product.stock;
      }

      ctrl.favorite = ctrl.product.favorite;
    };
  }
})();
