(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('ProductService', ProductService);

  ProductService.$inject = ['$http', '$q', 'API', 'ProductModel'];

  function ProductService($http, $q, API, ProductModel) {
    var service = {
      getProducts,
      getProduct,
      getUserProducts,
      getCategories,
      getSubCategories,
      addPrice,
      deletePrice,
      setProductFavorite,
      getFavoriteProducts,
      loadProduct,
      getProductAttributes,
      getAttributes,
      setProductAttributes,
      all
    };

    return service;

    ////////////////

    /**
     * Retrieves the products using the search parameter
     * @param  {String} search criteria
     */
    function getProducts(query) {
      const deferred = $q.defer();

      $http.get(API.getProducts, {params: query || {}})
        .then(response => {
          deferred.resolve(response.data.objects.map(ProductModel.create));
        });

      return deferred.promise;
    }

    /**
     * Retrieves a product using the input id
     * @param  {integer} id product id
     */
    function getProduct(id) {
      const deferred = $q.defer();

      $http.get(API.getProduct(id))
        .then(response => {
          deferred.resolve(response.data);
        });

      return deferred.promise;
    }

    /**
     * Retrieves the products that have a price added by the logged user
     */
    function getUserProducts(query) {
      const deferred = $q.defer();

      $http.get(API.getUserProducts, {params: query || {}})
        .then(response => deferred.resolve(response.data))
        .catch(() => deferred.reject());

      return deferred.promise;
    }

    /**
     * Retrieves the product categories
     */
    function getCategories() {
      const deferred = $q.defer();

      $http.get(API.getCategories)
        .then(response => {
          deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }

    function getAttributes() {
      const deferred = $q.defer();

      $http.get(API.getAttributes)
        .then(response => {
          deferred.resolve(response.data);
        });

      return deferred.promise;
    }

    /**
     * Retrieves the product sub categories
     */
    function getSubCategories(category) {
      const deferred = $q.defer();

      $http.get(API.getSubCategories, {params: category || {}})
        .then(response => {
          deferred.resolve(response.data.objects);
        });

      return deferred.promise;
    }

    function addPrice(data) {
      return $http.post(API.addProductPrice, data);
    }

    function deletePrice(data) {
      return $http.post(API.deleteProductPrice, data);
    }

    function setProductFavorite(data) {
      return $http.post(API.setProductFavorite, data);
    }

    function getFavoriteProducts(query) {
      const deferred = $q.defer();

      $http.get(API.getFavoriteProducts, {params: query || {}})
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function loadProduct(data) {
      const deferred = $q.defer();

      $http.post(API.loadProduct, data)
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function getProductAttributes(id) {
      const deferred = $q.defer();

      $http.get(API.getProductAttributes(id))
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function setProductAttributes(id, data) {
      return $http.post(API.setProductAttributes(id), data);
    }

    // TODO: check if it's not needed
    function all() {
      const promises = [
        getProducts(),
        getCategories(),
        getSubCategories()
      ];

      return $q.all(promises);
    }
  }
})();