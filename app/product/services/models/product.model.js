(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('ProductModel', ProductModel);

  function ProductModel() {
    class Product {

      constructor(data) {
        this.setData(data);
      }

      /**
       * Creates an instance of Product
       *
       * @param  {Object} data
       */
      static create(data) {
        return new Product(data);
      }

      /**
       * Sorts the list of prices
       */
      sortPrices() {
        this.prices = _.sortBy(this.prices, price => parseFloat(price.price));
      }

      /**
       * @param  {Object} data (json data from the server)
       */
      setData(data) {
        if (!_.isEmpty(data) && !_.isUndefined(data)) {
          angular.extend(this, data);

          // Format data
          this.sortPrices();
        }
      }
    }

    return Product;
  }

})();