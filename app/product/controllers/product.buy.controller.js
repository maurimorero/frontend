(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductBuyController', ProductBuyController);

  ProductBuyController.$inject = ['$window', '$stateParams', '$state', 'PurchaseModel', 'PurchasesService', 'ProductService', 'SessionService'];

  function ProductBuyController($window, $stateParams, $state, PurchaseModel, PurchasesService, ProductService, SessionService) {
    let vm = this;

    vm.pay = pay;
    vm.payment = PurchaseModel.create();
    vm.dataLoading = false;
    vm.product = {};
    vm.isProduct = _.get($state, 'current.data.type', 'product') === 'product';
    vm.stock = 0;
    vm.cancel = cancel;
    vm.status = {
      error: '',
      success: false,
      redirect: false
    };

    init();

    ////////////////

    function init() {
      const priceId = parseInt($stateParams.priceId, 10);
      const productId = parseInt($stateParams.productId, 10);

      if (vm.isProduct && productId) {
        ProductService.getProduct(productId)
          .then(product => {
            vm.product = product;
            const price = _.find(product.prices, {id: priceId});

            if (price) {
              vm.stock = price.stock;
            }
          });
      }
    }

    function pay() {
      vm.dataLoading = true;

      if (_.get(vm.payment, 'amount') > vm.stock) {
        vm.status.success = false;
        vm.status.error = 'Cantidad mayor al stock disponible';
        vm.dataLoading = false;
      } else {
        const sellerId = parseInt($stateParams.sellerId, 10);

        // TODO: use purchase model export data instead
        let data = {
          product_id: vm.product.id,
          count: _.get(vm.payment, 'amount'),
          seller_id: sellerId,
          buyer_id: SessionService.getUserId(),
          delivery: _.get(vm.payment, 'delivery'),
          receivers: vm.payment.receivers.filter(_.identity)
        };

        if (vm.user) {
          data = _.extend(data, vm.user);
        }

        if (vm.isProduct && vm.product.id) {
          data.product_id = vm.product.id;
          data.price_id = parseInt($stateParams.priceId, 10);
        } else {
          data.offer_id = parseInt($stateParams.offerId, 10);
        }

        PurchasesService.create(data)
          .then(response => {
            // vm.status.success = true;
            // vm.status.error = false;
            // vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = false;
            vm.status.redirect = true;

            $window.location.href = response.payment_url_ml;
          })
          .catch(error => {
            vm.status.success = false;
            vm.status.error = error;
            vm.dataLoading = false;
          });
      }
    }

    function cancel() {
      $window.history.back();
    }
  }
})();