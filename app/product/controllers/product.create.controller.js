(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductCreateController', ProductCreateController)
    .controller('DuplicatedProductModalCtrl', function ($uibModalInstance) {
      const $ctrl = this;

      $ctrl.ok = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    })
    .controller('ProductCreateModalCtrl', function ($uibModalInstance) {
      const $ctrl = this;

      $ctrl.okMP = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancelMP = function () {
        $uibModalInstance.dismiss('cancel');
      };
    });

  ProductCreateController.$inject = [
    'ProductService',
    'UserService',
    '$window',
    '$location',
    '$document',
    '$uibModal',
    '$state',
    'localStorageService'
  ];

  function ProductCreateController(ProductService, UserService, $window, $location, $document, $uibModal, $state, localStorageService) {
    const vm = this;

    vm.dataLoading = false;
    vm.status = {
      error: '',
      success: false
    };

    vm.step = 0;

    vm.product = {};
    vm.loadProduct = loadProduct;
    vm.getCategories = getCategories;
    vm.updateSubCategory = updateSubCategory;
    vm.validate = validate;
    vm.next = next;
    vm.save = save;
    vm.attributeSelected = {};

    init();

    ////////////////

    function init() {
      getCategories();
      validateMP();
    }

    function loadProduct() {
      const data = {
        category_id: vm.product.category.id,
        sub_category_id: vm.product.subCategory.id,
        name: vm.product.name,
        state: vm.product.type,
        price: vm.product.price,
        stock: vm.product.stock,
        image_1: vm.product.image_1,
        image_2: vm.product.image_2,
        image_3: vm.product.image_3,
        image_4: vm.product.image_4
      };

      return ProductService.loadProduct(data);
    }

    function getCategories() {
      ProductService.getCategories()
        .then(categories => vm.categories = categories);
    }

    function updateSubCategory() {
      let query = {
        category_id: _.get(vm.product, 'categories.id')
      };

      ProductService.getSubCategories(query)
        .then(subCategories => vm.subCategories = subCategories);
    }

    function next() {
      vm.dataLoading = true;

      loadProduct().then(product => {
        vm.productId = product.id;

        if (!product.created) {
          open();
        } else {
          vm.step = 1;
          return ProductService.getProductAttributes(product.id);
        }
      })
      .then(attributes => {
        const byGroup = _.groupBy(attributes, 'group');
        vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));

        vm.dataLoading = false;
      })
      .catch(() => {
        vm.dataLoading = false;
        vm.status.success = false;
        vm.status.error = 'Error al guardar los datos';
      });
    }

    function open(parentSelector) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContent.html',
        controller: 'DuplicatedProductModalCtrl',
        controllerAs: 'md',
        appendTo: parentElem
      });

      modalInstance.result.then(function () {
        $state.go('productDetail', {id: vm.productId});
      }, function () {});
    }

    function openMP(parentSelector) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContentMP.html',
        controller: 'ProductCreateModalCtrl',
        controllerAs: 'md',
        appendTo: parentElem
      });

      modalInstance.result.then(function () {
        localStorageService.add('productMP', true);
        $state.go('profile');
      }, function () {});
    }

    function save() {
      vm.dataLoading = true;

      const data = {
        attrs: _.map(vm.attributeSelected, v => ({
          value: true, attribute_id: parseInt(v)
        })),
        description: vm.description
      };

      ProductService.setProductAttributes(vm.productId, data)
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';
        })
        .catch(() => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = 'Error al guardar los datos';
        });
    }

    function validate() {
      return _.isUndefined(vm.product.type) || !_.some([
        vm.product.image_1,
        vm.product.image_2,
        vm.product.image_3,
        vm.product.image_4
      ]);
    }

    function validateMP() {
      return UserService.getUser().then(data => {
        if (!data.is_authorized_by_ml) {
          openMP();
        } else {
          const productMP = localStorageService.remove('productMP');
          if (productMP) {
            const search = new URL($location.absUrl());
            $window.location.replace(`${search.origin}${search.pathname}`);
          }
        }
      });
    }
  }
})();

