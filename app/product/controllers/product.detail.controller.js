(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductDetailController', ProductDetailController)
    .controller('ModalInstanceCtrl', function ($uibModalInstance, product) {
      const $ctrl = this;

      $ctrl.product = product;

      $ctrl.ok = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    });

  ProductDetailController.$inject = [
    '$stateParams',
    'ProductService',
    'SessionService',
    '$uibModal',
    '$document',
    '$state',
    '$window',
    '$location'
  ];

  function ProductDetailController($stateParams, ProductService, SessionService, $uibModal, $document, $state, $window, $location) {
    const vm = this;

    vm.enableAnswers = false;
    vm.userRole = SessionService.getUserRole();
    vm.loggedIn = SessionService.userLoggedIn();
    vm.addFavorite = addFavorite;
    vm.favorite = false;
    vm.open = open;
    vm.currentURL = $location.absUrl();

    vm.print = function () {
      $window.print();
    };

    vm.status = {
      success: false,
      error: ''
    };

    init();

    ////////////////

    function init() {
      const productId = $stateParams.id;
      const userId = SessionService.getUserId();

      if (productId) {
        ProductService.getProduct(productId)
          .then(product => {
            vm.product = product;
            vm.prices = _.filter(product.prices, price => price.stock > 0);
            vm.favorite = product.favorite;
            vm.sellerPrice = _.find(product.prices, {seller_id: userId});
            vm.priceLoadedByUser = !_.isUndefined(vm.sellerPrice);

            const byGroup = _.groupBy(product.attributes, 'group');
            vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));
            vm.emptyAttributes = _.isEmpty(vm.attributes);

            if (vm.product.image_1) {
              vm.productImage = vm.product.image_1;
            } else {
              vm.productImage = vm.product.default_image;
            }
          });
      }
    }

    function addFavorite(productId) {
      if (vm.loggedIn) {
        ProductService.setProductFavorite({product_id: productId});
        vm.favorite = !vm.favorite;
      } else {
        $state.go('login');
      }
    }

    function open(parentSelector) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContent.html',
        controller: 'ModalInstanceCtrl',
        controllerAs: 'md',
        appendTo: parentElem,
        resolve: {
          product: function () {
            return vm.product;
          }
        }
      });

      modalInstance.result.then(function () {
        if (vm.priceLoadedByUser) {
          let data = {
            price_id: parseInt(vm.sellerPrice.id, 10)
          };

          ProductService.deletePrice(data)
            .then(() => {
              $state.go('myProducts');
            })
            .catch(() => {
              vm.status.success = false;
              vm.status.error = 'al eliminar el producto';
            });
        }

      }, function () {});
    }
  }
})();
