(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductInfoController', ProductInfoController);

  ProductInfoController.$inject = ['$stateParams', 'ProductService'];

  function ProductInfoController($stateParams, ProductService) {
    var vm = this;

    init();

    ////////////////

    function init() {
      const productId = $stateParams.id;

      if (productId) {
        ProductService
          .getProduct(productId)
          .then(product => {
            vm.product = product;

            const byGroup = _.groupBy(product.attributes, 'group');
            vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));
          });
      }
    }
  }
})();
