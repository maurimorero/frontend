(function () {
  'use strict';

  angular
    .module('mlaApp')
    .controller('ProductPricesListController', ProductPricesListController);

  ProductPricesListController.$inject = ['$stateParams', 'ProductService'];
  function ProductPricesListController($stateParams, ProductService) {
    const vm = this;

    vm.pagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 20
    };

    init();

    ////////////////

    function init() {
      const productId = $stateParams.id;

      ProductService.getProduct(productId)
        .then(product => {
          vm.product = product;
          vm.prices = _.filter(product.prices, price => price.stock > 0);
          vm.pagination.totalItems = vm.prices;
        });
    }
  }
})();