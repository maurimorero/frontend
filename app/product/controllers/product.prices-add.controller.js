(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductPricesAddController', ProductPricesAddController)
    .controller('ModalMPCtrl', function ($uibModalInstance, title) {
      const $ctrl = this;

      $ctrl.title = title;

      $ctrl.ok = function () {
        $uibModalInstance.close();
      };

      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    });;

  ProductPricesAddController.inject = [
    '$stateParams',
    'ProductService',
    'SessionService',
    'UserService',
    '$state',
    'localStorageService',
    '$uibModal'
  ];

  function ProductPricesAddController($stateParams, ProductService, SessionService, UserService, $state, localStorageService, $uibModal) {
    const vm = this;

    vm.updatePrice = _.get($state, 'current.data.update', false);

    vm.status = {
      error: '',
      success: false
    };

    vm.addPrice = addPrice;
    vm.back = back;

    init();

    ////////////////

    function init() {}

    function addPrice() {
      UserService.getUser().then(data => {
        if (validateMP(data)) {
          addPriceFinal();
        }
      });
    }

    function addPriceFinal() {
      vm.dataLoading = true;
      const productId = $stateParams.productId;

      let data = {
        seller_id: SessionService.getUserId(),
        product_id: parseInt(productId, 10),
        price: vm.price.replace(',', '.'),
        stock: vm.stock
      };

      ProductService.addPrice(data)
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';
        })
        .catch(err => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = err.data && err.data.error;
        });
    }

    function back() {
      $state.go('productDetail', {id: $stateParams.productId});
    }

    function open(parentSelector, title) {
      const parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

      const modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalContent.html',
        controller: 'ModalMPCtrl',
        controllerAs: 'md',
        appendTo: parentElem,
        resolve: {
          title: function () {
            return title;
          }
        }
      });

      return modalInstance.result;
    }

    function validateMP(data) {
      if (data.is_authorized_by_ml) {
        localStorageService.remove('productOfferMP');
        return true;
      } else {
        open(null, 'Debe tener Mercado Pago activo antes de agregar un precio')
          .then(function () {
            localStorageService.add('productOfferMP', {
              id: $stateParams.productId,
            });
            $state.go('profile');
          }, function () {});

        return false;
      }
    }
  }
})();
