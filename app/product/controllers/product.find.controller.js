(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProductFindController', ProductFindController);

  ProductFindController.$inject = ['ProductService', 'SessionService', '$state', '$location', 'PAGINATION'];

  function ProductFindController(ProductService, SessionService, $state, $location, PAGINATION) {
    const vm = this;

    vm.userRole = SessionService.getUserRole();
    vm.userLoggedIn = SessionService.userLoggedIn();
    vm.dataLoading = false;
    vm.showMyProducts = _.get($state, 'current.data.myProducts', false);
    vm.showFavoriteProducts = _.get($state, 'current.data.favorites', false);
    vm.productsList = [];
    vm.productsListByPreference = [];
    vm.products = [];
    vm.categories = [];
    vm.subCategories = [];
    vm.grid = true;
    vm.product = {
      attrs: [],
      order_by: '-date_created'
    };

    vm.myProduct = {
      state: '',
      order: ''
    };

    vm.isHome = true;

    if ($location.$$url !== '/home') {
      vm.isHome = false;
    }

    vm.setGridView = function (view) {
      vm.grid = view;
    };

    // https://stackoverflow.com/questions/19809717/ng-select-gives-only-string-but-i-want-integer

    vm.pagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    vm.pPagination = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    vm.find = findProducts;
    vm.findUserProducts = findUserProducts;
    vm.getFavoriteProducts = getFavoriteProducts;
    vm.getSubCategories = getSubCategories;
    vm.selectAttibute = selectAttibute;

    init();

    ////////////////

    function init() {
      // getCategories();
      getAttributes();
      getSubCategories();

      if (vm.showMyProducts) {
        findUserProducts();
      } else if (vm.showFavoriteProducts) {
        getFavoriteProducts();
      } else {
        findProducts();
        vm.userLoggedIn && findProductsByPreference();
      }
    }

    function findProducts(keyEvent) {
      if (keyEvent && keyEvent.which !== 13) return;

      vm.dataLoading = true;

      if (vm.showMyProducts) {
        findUserProducts();
      } else {
        const attrs = _.get(vm.product, 'attrs');

        const get = vm.showFavoriteProducts
          ? ProductService.getFavoriteProducts
          : ProductService.getProducts;

        let query = {
          category_id: _.get(vm.product, 'categories.id'),
          sub_category_id: _.get(vm.product, 'subCategory.id'),
          attrs_id: _.isEmpty(attrs) ? [] : _.compact(attrs).join(','),
          order_by: _.get(vm.product, 'order_by')
        };

        if (vm.showFavoriteProducts) {
          query.search = _.get(vm.product, 'product');
        } else {
          query.name__contains = _.get(vm.product, 'product');
        }

        get(query)
          .then(products => {
            vm.pagination.totalItems = _.size(products);
            vm.productsList = products;
            vm.dataLoading = false;
          });
      }
    }

    function findProductsByPreference() {
      /**
       * By adding the user_id in the query it is possible to filter the
       * productcs by the user preferences
       */

      vm.dataLoading = true;

      let query = {
        user_id: SessionService.getUserId()
      };

      ProductService.getProducts(query)
        .then(products => {
          vm.productsListByPreference = products;
          vm.pPagination.totalItems = _.size(products);
          vm.dataLoading = false;
        });
    }

    const setStatus = status => {
      if (status === 'A') {
        return 'publicado';
      } else if (status === 'P') {
        return 'pendiente';
      } else if (status === 'R') {
        return 'rechazado';
      }
    };

    function findUserProducts() {
      vm.dataLoading = true;

      let query = {};

      const state = _.get(vm.myProduct, 'state');
      const order = _.get(vm.myProduct, 'order');

      if (state) {
        const filter = state.split('=')[0];
        const value = state.split('=')[1];
        query[filter] = value;
      }

      if (order) {
        const sort = order.split('=')[0];
        const value = order.split('=')[1];
        query[sort] = value;
      }

      ProductService.getUserProducts(query)
        .then(products => {
          _.each(products, product => {
            product.status = setStatus(product.request_status);
            product.selfProduct = vm.showMyProducts;
          });

          vm.pagination.totalItems = _.size(products);
          vm.productsList = products;
          vm.dataLoading = false;
        })
        .catch(() => {
          vm.dataLoading = false;
        });
    }

    function getFavoriteProducts(keyEvent) {
      if (keyEvent && keyEvent.which !== 13) return;

      vm.dataLoading = true;

      let query = {
        search: _.get(vm.product, 'product')
      };

      ProductService.getFavoriteProducts(query)
        .then(products => {
          vm.pagination.totalItems = _.size(products);
          vm.productsList = products;
          vm.dataLoading = false;
        });
    }

    function getAttributes() {
      ProductService.getAttributes()
        .then(attributes => {
          vm.attributes = _.groupBy(_.uniqWith(attributes, _.isEqual), 'feature');
        });
    }

    function getSubCategories() {
      ProductService
        .getSubCategories()
        .then(subCategories => vm.subCategories = subCategories);

    }

    function selectAttibute(id, options) {
      const optionIds = _.map(options, 'id');
      _.remove(vm.product.attrs, id => optionIds.includes(id));
      vm.product.attrs[id] = id;
      vm.find();
    }

  }
})();