(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('SaleModel', SaleModel);


  function SaleModel() {
    class Sale {

      constructor(data) {
        this.setData(data);
      }

      /**
       * Creates an instance of Sale
       *
       * @param  {Object} data
       */
      static create(data) {
        return new Sale(data);
      }

      /**
       * @param  {Object} data (json data from the server)
       */
      setData(data) {
        if (!_.isEmpty(data) && !_.isUndefined(data)) {
          angular.extend(this, data);
        }
      }
    }

    return Sale;
  }

})();
