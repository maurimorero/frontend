(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('SalesService', SalesService);

  SalesService.$inject = ['$q', '$http', 'API', 'SaleModel', 'SessionService'];

  function SalesService($q, $http, API, SaleModel, SessionService) {
    const service = {
      get,
      getComplaintsSeller
    };

    return service;

    ////////////////

    function get() {
      const deferred = $q.defer();

      $http.get(API.getSales(SessionService.getUserId()))
        .then(response => deferred.resolve(response.data.objects.map(SaleModel.create)));

      return deferred.promise;
    }

    function getComplaintsSeller(query) {
      const deferred = $q.defer();

      $http.get(API.getComplaintsSeller, {params: query || {}})
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }
  }
})();
