(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('SaleStateController', SaleStateController);

  SaleStateController.inject = ['$stateParams', 'PurchasesService'];

  function SaleStateController($stateParams, PurchasesService) {
    const vm = this;

    vm.purchaseId = parseInt($stateParams.id, 10);

    init();

    ////////////////

    function init() {
      if (vm.purchaseId) {
        PurchasesService.detail(vm.purchaseId)
          .then(purchase => {
            vm.purchase = purchase;

            if (purchase.receivers_data.length) {
              vm.receivers = purchase.receivers_data;
            } else {
              vm.receivers = [purchase.buyer_data];
            }
          });
      }
    }
  }
})();
