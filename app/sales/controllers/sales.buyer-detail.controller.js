(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('SaleBuyerDetailController', SaleBuyerDetailController);

  SaleBuyerDetailController.inject = ['$stateParams', 'UserService'];

  function SaleBuyerDetailController($stateParams, UserService) {
    const vm = this;


    init();

    /////////////////

    function init() {
      const buyerId = $stateParams.id;

      if (buyerId) {
        UserService.getUser(buyerId)
          .then(user => {
            vm.buyer = user;
            return UserService.getUserRating(buyerId);
          })
          .then(metrics => {
            vm.buyer.rating = _.range(metrics.reputation);
            vm.buyer.purchases = metrics.purchases;
          });
      }
    }
  }
})();
