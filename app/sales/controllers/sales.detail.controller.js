(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('SalesDetailController', SalesDetailController);

  SalesDetailController.$inject = ['$stateParams', 'PurchasesService', '$window', 'UserService'];

  function SalesDetailController($stateParams, PurchasesService, $window, UserService) {
    const vm = this;

    vm.purchaseId = parseInt($stateParams.id, 10);

    vm.buyer = {};

    vm.print = function () {
      $window.print();
    };

    init();

    ////////////////

    function init() {
      if (vm.purchaseId) {
        PurchasesService.detail(vm.purchaseId)
          .then(purchase => {
            vm.purchase = purchase;
            vm.sent = displaySent(vm.purchase.state.state);
            vm.grade = gradeBuyer(vm.purchase.state.state) && !purchase.has_buyer_calification;

            const byGroup = _.groupBy(purchase.attributes, 'group');
            vm.attributes = _.mapValues(byGroup, group => _.groupBy(group, 'feature'));
            vm.attributesLen = _.size(vm.attributes);

            if (purchase.receivers_data.length) {
              vm.receivers = purchase.receivers_data;
            } else {
              vm.receivers = [purchase.buyer_data];
            }

            return vm.purchase.buyer_id;
          })
          .then(buyer_id => {
            return UserService.getUser(buyer_id);
          })
          .then(user => {
            vm.buyer = user;
          })
          .then(() => {
            return UserService.getUserRating(vm.purchase.buyer_id);
          })
          .then(metrics => {
            vm.buyer.rating = _.range(metrics.buyer_reputation);
            vm.buyer.sales = metrics.sales;
          });
      }
    }

    function displaySent(state) {
      if (state === 'En progreso') {
        return true;
      } else if (state === 'Enviada') {
        return false;
      } else if (state === 'Exitosa') {
        return false;
      } else if (state === 'En conflicto') {
        return false;
      } else if (state === 'Cancelada') {
        return false;
      }
    }

    function gradeBuyer(state) {
      if (state === 'En progreso') {
        return false;
      } else if (state === 'Enviada') {
        return false;
      } else if (state === 'Exitosa') {
        return true;
      } else if (state === 'En conflicto') {
        return false;
      } else if (state === 'Cancelada') {
        return false;
      }
    }
}
})();
