(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('SaleSentConfirmationController', SaleSentConfirmationController);

  SaleSentConfirmationController.inject = ['$stateParams', 'PurchasesService'];

  function SaleSentConfirmationController($stateParams, PurchasesService) {
    const vm = this;

    vm.deliveryOptions = [];
    vm.dataLoading = false;
    vm.status = {
      error: '',
      success: false
    };

    vm.confirm = confirm;

    init();

    ////////////////

    function init() {
      getDeliveryTypes();
    }

    function getDeliveryTypes() {
      PurchasesService.getDeliveryTypes()
        .then(options => vm.deliveryOptions = options);
    }

    function confirm() {
      vm.dataLoading = true;
      const purchaseId = parseInt($stateParams.purchaseId, 10);

      const query = {
        purchase_id: purchaseId,
        type_sent_id: vm.deliveryMethod.id,
        tracking_code: vm.code
      };

      PurchasesService.setPurchaseSentState(query)
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
        })
        .catch((err) => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = err;
        });
    }
  }

})();
