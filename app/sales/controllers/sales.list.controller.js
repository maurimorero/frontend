(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('SalesListController', SalesListController);

  SalesListController.inject = ['PurchasesService', 'SalesService', 'SessionService'];
  function SalesListController(PurchasesService, SalesService, SessionService) {
    const vm = this;

    vm.search = search;
    vm.grid = true;
    vm.stateSelected = '';

    vm.setGridView = function (view) {
      vm.grid = view;
    };

    vm.saleSort = {};

    vm.pSales = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: 20
    };

    init();

    ////////////////

    function init() {
      SalesService.get().then(sales => {
        vm.pSales.totalItems = _.size(sales);
        vm.sales = sales;
      });
    }

    function search() {
      let query = {
        seller_id: SessionService.getUserId()
      };

      const sortValue = _.get(vm.saleSort, 'state');

      if (vm.stateSelected) {
        query.state = vm.stateSelected;
      }

      if (sortValue) {
        query.order_by = sortValue;
      }

      if (vm.stateSelected || sortValue) {
        PurchasesService.filterByState(query)
          .then(sales => {
            vm.pSales.totalItems = _.size(sales);
            vm.sales = sales;
          });
      }
    }
  }
})();