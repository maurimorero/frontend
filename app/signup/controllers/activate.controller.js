(function () {
  'use strict';

  angular
    .module('mlaApp')
    .controller('ActivateController', ActivateController);

    ActivateController.$inject = ['$stateParams', 'AuthenticationService'];

    function ActivateController($stateParams, AuthenticationService) {
      const vm = this;
      const userId = _.get($stateParams, 'userId', '');
      const code = _.get($stateParams, 'code', '');

      activate(userId, code);

      vm.activate = activate;
      vm.dataLoading = false;

      vm.status = {
        success: false,
        error: ''
      };

      //////////////////

      /**
       * Activate method
       */
      function activate(userId, code) {
        vm.dataLoading = true;

        AuthenticationService.activate(userId, code)
          .then(() => {
            vm.dataLoading = false;
            vm.status.success = true;
          })
          .catch(err => {
            vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = err.data && err.data.error;
          });
      }
    }
})();