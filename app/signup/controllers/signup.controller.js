(function () {
  'use strict';

  angular
    .module('mlaApp')
    .controller('SignupController', SignupController);

    SignupController.$inject = ['UserService'];

    function SignupController(UserService) {
      const vm = this;

      vm.signup = signup;
      vm.dataLoading = false;
      vm.status = {
        success: false,
        error: ''
      };

      //////////////////

      /**
       * Sign up method
       */
      function signup() {
        vm.dataLoading = true;

        UserService.createUser(vm.user)
          .then(() => {
            vm.dataLoading = false;
            vm.status.success = true;
          })
          .catch(err => {
            vm.dataLoading = false;
            vm.status.success = false;
            vm.status.error = err.data && err.data.error;
          });
      }
    }
})();