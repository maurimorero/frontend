(function() {
  'use strict';

  angular
    .module('mlaApp')
    .controller('PurchaseStateController', PurchaseStateController);

  PurchaseStateController.$inject = ['$stateParams', '$state', 'PurchasesService'];
  function PurchaseStateController($stateParams, $state, PurchasesService) {
    const vm = this;

    vm.purchaseId = parseInt($stateParams.id, 10);
    vm.state = _.get($state, 'current.data.state');

    vm.success = false;

    init();

    ////////////////

    function init() {
      if (vm.purchaseId && vm.state) {
        updatePurchaseState().then(() => vm.success = true);
      }
    }

    function updatePurchaseState() {
      return PurchasesService.updatePurchaseState(vm.purchaseId, vm.state);
    }
  }
})();