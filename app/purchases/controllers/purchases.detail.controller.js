(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('PurchasesDetailController', PurchasesDetailController);

  PurchasesDetailController.$inject = ['$stateParams', '$window', '$location', 'PurchasesService', 'UserService'];

  function PurchasesDetailController($stateParams, $window, $location, PurchasesService, UserService) {
    var vm = this;

    vm.purchaseId = $stateParams.id;
    vm.seller = {};
    vm.currentURL = $location.absUrl();

    vm.print = function () {
      $window.print();
    };

    init();

    ////////////////

    function displayConfirmation(state) {
      if (state === 'En progreso') {
        return false;
      } else if (state === 'Enviada') {
        return true;
      } else if (state === 'Exitosa') {
        return false;
      } else if (state === 'En conflicto') {
        return true;
      } else if (state === 'Cancelada') {
        return false;
      }
    }

    function displayConflict(state) {
      if (state === 'En progreso') {
        return true;
      } else if (state === 'Enviada') {
        return true;
      } else if (state === 'Exitosa') {
        return false;
      } else if (state === 'En conflicto') {
        return false;
      } else if (state === 'Cancelada') {
        return false;
      }
    }

    function init() {
      // detalle compra
      if (vm.purchaseId) {
        PurchasesService.detail(vm.purchaseId)
          .then(purchase => {
            vm.purchase = purchase;
            vm.displayConfirmation = displayConfirmation(purchase.state.state);
            vm.displayConflict = displayConflict(purchase.state.state);

            // vm.complaintDone = !!_.size(vm.purchase.complaints);

            return purchase.seller_id;
          })
          .then(seller_id => {
            return UserService.getUser(seller_id);
          })
          .then(user => {
            vm.seller = user;
          })
          .then(() => {
            return UserService.getUserRating(vm.purchase.seller_id);
          })
          .then(metrics => {
            vm.seller.rating = _.range(metrics.seller_reputation);
            vm.seller.sales = metrics.sales;
          });
      }
    }
}
})();
