(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('PurchasesComplaintController', PurchasesComplaintController);

  PurchasesComplaintController.inject = ['$stateParams', 'PurchasesService'];

  function PurchasesComplaintController($stateParams, PurchasesService) {
    const vm = this;

    vm.save = save;
    vm.options = {};
    vm.status = {
      error: '',
      success: false
    };

    init();

    ////////////////

    function init() {}

    function save() {
      vm.dataLoading = true;
      const purchaseId = $stateParams.purchaseId;

      const data = {
        purchase_id: purchaseId,
        motive: vm.comments,
        complaint_type: vm.option
      };

      PurchasesService.setComplaint(data)
        .then(() => {
          vm.status.success = true;
          vm.status.error = false;
          vm.dataLoading = false;
        })
        .catch(error => {
          vm.status.success = false;
          vm.status.error = error;
          vm.dataLoading = false;
        });
    }
  }
})();