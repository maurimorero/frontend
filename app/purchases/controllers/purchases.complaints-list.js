(function () {
  'use strict';

    angular
      .module('mlaApp')
      .controller('ComplaintsController', ComplaintsController);

    ComplaintsController.inject = ['$q', 'PurchasesService', 'SalesService', 'SessionService', 'PAGINATION'];
    function ComplaintsController($q, PurchasesService, SalesService, SessionService, PAGINATION) {
      const vm = this;

      vm.userRole = SessionService.getUserRole();
      vm.complaints = [];
      vm.grid = true;
      vm.search = search;

      vm.complaint = {
        sort_by: 'date'
      };

      vm.setGridView = function (view) {
        vm.grid = view;
      };

      vm.pagination = {
        totalItems: 0,
        currentPage: 1,
        itemsPerPage: PAGINATION.amount
      };

      init();

      ////////////////

      function search() {
        const query = {
          state: _.get(vm.complaint, 'state'),
          sort_by: _.get(vm.complaint, 'sort_by')
        };

        const get = vm.userRole === 'seller'
          ? SalesService.getComplaintsSeller
          : PurchasesService.getComplaintsBuyer;

        get(query).then(complaints => {
          vm.pagination.totalItems = _.size(complaints);
          vm.complaints = complaints;
        });
      }

      function init() {
        search();
      }
    }
  })();
