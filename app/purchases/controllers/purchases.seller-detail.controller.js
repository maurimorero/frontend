(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('PurchasesSellerDetailController', PurchasesSellerDetailController);

  PurchasesSellerDetailController.inject = ['$stateParams', 'UserService'];

  function PurchasesSellerDetailController($stateParams, UserService) {
    const vm = this;


    init();

    /////////////////

    function init() {
      const sellerId = $stateParams.id;

      if (sellerId) {
        UserService.getUser(sellerId)
          .then(user => {
            vm.seller = user;
            return UserService.getUserRating(sellerId);
          })
          .then(metrics => {
            vm.seller.rating = _.range(metrics.reputation);
            vm.seller.sales = metrics.sales;
          });
      }
    }
  }
})();
