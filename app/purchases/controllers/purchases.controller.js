(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('PurchasesController', PurchasesController);

  PurchasesController.$inject = ['$stateParams', '$state', 'PurchasesService', 'SessionService', 'PAGINATION'];

  function PurchasesController($stateParams, $state, PurchasesService, SessionService, PAGINATION) {
    const vm = this;

    vm.purchase = {
      order_by: 'date_created'
    };
    vm.search = search;
    vm.grid = true;
    vm.stateSelected = '';

    vm.setGridView = function (view) {
      vm.grid = view;
    };

    vm.pPurchases = {
      totalItems: 0,
      currentPage: 1,
      itemsPerPage: PAGINATION.amount
    };

    init();

    ////////////////
    function init() {
        getPurchases();
    }

    function getPurchases() {
      PurchasesService.get()
        .then(purchases => {
          vm.pPurchases.totalItems = _.size(purchases);
          vm.purchases = purchases;
        });
    }

    function search() {
      let query = {
        buyer_id: SessionService.getUserId(),
        order_by: _.get(vm.purchase, 'order_by'),
        state: vm.stateSelected
      };

      PurchasesService.filterByState(query)
          .then(purchases => {
            vm.pPurchases.totalItems = _.size(purchases);
            vm.purchases = purchases;
          });
    }
  }
})();
