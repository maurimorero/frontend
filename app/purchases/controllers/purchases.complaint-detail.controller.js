(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('PurchasesComplaintDetailController', PurchasesComplaintDetailController);

  PurchasesComplaintDetailController.inject = ['$window', '$stateParams', '$location', 'PurchasesService', 'SessionService'];

  function PurchasesComplaintDetailController($window, $stateParams, $location, PurchasesService, SessionService) {
    const vm = this;

    vm.userRole = SessionService.getUserRole();
    vm.currentURL = $location.absUrl();

    vm.print = function () {
      $window.print();
    };

    init();

    ////////////////

    function init() {
      const complaintId = parseInt($stateParams.complaintId, 10);
      PurchasesService.getComplaint(complaintId)
        .then(complaint => {
          vm.complaint = complaint;
        });
    }
  }
})();