(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('PurchasesService', PurchasesService);

  PurchasesService.$inject = ['$q', '$http', 'API', 'PurchaseModel', 'SessionService'];

  function PurchasesService($q, $http, API, PurchaseModel, SessionService) {
    const service = {
      create,
      get,
      detail,
      setSellerRating,
      setBuyerRating,
      getDeliveryTypes,
      setPurchaseSentState,
      filterByState,
      setComplaint,
      getComplaint,
      getComplaintQuestions,
      createComplaintQuestion,
      createComplaintReply,
      getComplaintsBuyer,
      updatePurchaseState
    };

    return service;

    ////////////////

    function get() {
      const deferred = $q.defer();

      $http.get(API.getPurchases(SessionService.getUserId()))
        .then(response => deferred.resolve(response.data.objects.map(PurchaseModel.create)));

      return deferred.promise;
    }

    function create(data) {
      const deferred = $q.defer();

      $http.post(API.createPurchase, data)
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function detail(id) {
      const deferred = $q.defer();

      $http.get(API.detailPurchase(id))
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function setSellerRating(data) {
      return $http.post(API.setPurchaseRating, data);
    }

    function setBuyerRating(data) {
      return $http.post(API.setSaleRating, data);
    }

    function getDeliveryTypes() {
      const deferred = $q.defer();

      $http.get(API.getDeliveryTypes)
        .then(response => deferred.resolve(response.data.objects));

      return deferred.promise;
    }

    function setPurchaseSentState(data) {
      return $http.post(API.setPurchaseSentState, data);
    }

    function filterByState(query) {
      const deferred = $q.defer();

      $http.get(API.filterPurchaseByState, {params: query || {}})
        .then(response => deferred.resolve(response.data.objects.map(PurchaseModel.create)));

      return deferred.promise;
    }

    function setComplaint(data) {
      return $http.post(API.purchaseComplaint, data);
    }

    function getComplaint(id) {
      const deferred = $q.defer();

      $http.get(API.getComplaint(id))
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function getComplaintQuestions(id) {
      const deferred = $q.defer();

      $http.get(API.getComplaintQuestions(id))
        .then(response => deferred.resolve(response.data.objects));

      return deferred.promise;
    }

    function getComplaintsBuyer(query) {
      const deferred = $q.defer();

      $http.get(API.getComplaintsBuyer, {params: query || {}})
        .then(response => deferred.resolve(response.data));

      return deferred.promise;
    }

    function createComplaintQuestion(data) {
      return $http.post(API.createComplaintQuestion, data);
    }

    function createComplaintReply(data) {
      return $http.post(API.createComplaintReply, data);
    }

    function updatePurchaseState(id, state) {
      return $http.post(API.updateState, {purchase_id: id, state: state});
    }
  }
})();
