(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('PurchaseModel', PurchaseModel);

  function PurchaseModel() {
    class Purchase {

      constructor(data) {
        this.setData(data);
        this.receivers = [''];
      }

      /**
       * Creates an instance of Purchase
       *
       * @param  {Object} data
       */
      static create(data) {
        return new Purchase(data);
      }

      /**
       * Adds alternative receivers
       */
      addReceiver() {
        this.receivers.push('');
      }

      /**
       * Removes alternative receivers
       */
      removeReceiver() {
        this.receivers.pop();
      }

      /**
       * @param  {Object} data (json data from the server)
       */
      setData(data) {
        if (!_.isEmpty(data) && !_.isUndefined(data)) {
          angular.extend(this, data);
        }
      }
    }

    return Purchase;
  }

})();