(function () {
'use strict';

  angular
    .module('mlaApp')
    .constant('API', {
      // Login
      login: '/api/v1/user/login/',
      logout: '/api/v1/user/logout/',
      changePassword: '/api/v1/user/change-password/',
      resetPassword: '/api/v1/user/reset-user/',
      newPassword: '/api/v1/user/reset-password/',
      authMl: '/api/v1/user/authorization-ml/',

      // Users
      register: '/api/v1/user/signup/',
      getUser: function (id) {
        return '/api/v1/user/' + id + '/';
      },
      updateUser: function (id) {
        return '/api/v1/user/update-profile/' + id + '/';
      },
      activate: '/api/v1/user/activate-user/',
      updateUserPreferences: '/api/v1/user-preferences/update-user-preferences/',
      getUserPreferences: '/api/v1/user-preferences/get-user-preferences/',
      getUserActivity: '/api/v1/activitylog/user-action-logs/',
      getUserRating: function (id) {
        return '/api/v1/user/reputation/' + id + '/';
      },

      disableMP: '/api/v1/user/inactive-ml/',

      // Products
      getCategories: '/api/v1/category/',
      getSubCategories: '/api/v1/subcategory/',
      getProducts: '/api/v1/product/',
      addProductPrice: '/api/v1/product/set-price/',
      deleteProductPrice: '/api/v1/product/delete-price/',
      getProductAttributes: function (id) {
        return '/api/v1/product/get-product-attributes/' + id + '/';
      },
      getAttributes: '/api/v1/product/get-attributes/',
      setProductAttributes: function (id) {
        return '/api/v1/product/set-product-attributes/' + id + '/';
      },
      getProduct: function (id) {
        return '/api/v1/product/' + id + '/';
      },
      getUserProducts: '/api/v1/product/get-user-products/',
      setProductFavorite: '/api/v1/product/set-product-as-favorite/',
      getFavoriteProducts: '/api/v1/product/get-favorite-products/',
      loadProduct: '/api/v1/product/load-product/',

      // Quotations
      createQuotation: '/api/v1/estimation/create-generic-estimation/',
      createCustomQuotation: '/api/v1/custom-estimation/create-custom-estimation/',
      getQuotations: '/api/v1/estimation/',
      getCustomQuotations: '/api/v1/custom-estimation/',
      addQuotationOffer: '/api/v1/estimation/estimation-create-offer/',
      addCustomQuotationOffer: '/api/v1/custom-estimation/custom-estimation-create-offer/',
      getQuotation: function (id) { // quotation detail
        return '/api/v1/estimation/' + id;
      },
      getCustomQuotation: function (id) { // custom quotation detail
        return '/api/v1/custom-estimation/' + id;
      },
      getQuotationAttributes: function (id) {
        return '/api/v1/custom-estimation/get-estimation-attributes/' + id + '/';
      },

      setQuotationAttributes: function (id) {
        return '/api/v1/custom-estimation/set-estimation-attributes/' + id + '/';
      },

      myOffers: '/api/v1/offer/',
      cancelOffer: function (id) {
        return '/api/v1/offer/cancel-offer/' + id + '/';
      },

      // Purchases
      getPurchases: function (id) {
        return '/api/v1/purchase/?buyer_id=' + id;
      },
      createPurchase: '/api/v1/purchase/create-purchase/',
      setPurchaseRating: '/api/v1/purchase/buyer-calification/',
      setSaleRating: '/api/v1/purchase/seller-calification/',
      detailPurchase: function (id) {
        return '/api/v1/purchase/' + id + '/';
      },
      getDeliveryTypes: '/api/v1/type-sent/',
      setPurchaseSentState: '/api/v1/purchase/set-state-to-sent/',
      filterPurchaseByState: '/api/v1/purchase/',
      purchaseComplaint: '/api/v1/complaint/create-complaint/',
      getComplaint: function (id) {
        return '/api/v1/complaint/' + id + '/';
      },

      getComplaintQuestions: function (id) {
        return '/api/v1/complaint-question/?format=json&complaint_id=' + id;
      },

      getComplaintsBuyer: '/api/v1/complaint/complaints-buyer/',
      getComplaintsSeller: '/api/v1/complaint/complaints-seller/',
      createComplaintQuestion: '/api/v1/complaint-question/create-question/',
      createComplaintReply: '/api/v1/complaint-question/create-reply/',
      updateState: '/api/v1/purchase/ml-update-state/',

      // Sales
      getSales: function (id) {
        return '/api/v1/purchase/?seller_id=' + id;
      },

      // Questions
      createQuestion: '/api/v1/question/create-question/',
      createReply: '/api/v1/question/create-reply/',
      getQuotationsQuestions: function (id) {
        return '/api/v1/question/?estimation_id=' + id;
      },
      getCustomQuotationsQuestions: function (id) {
        return '/api/v1/question/?custom_estimation_id=' + id;
      },
      getQuestions: '/api/v1/question/questions-received/',
      getQuestion: function (id) {
        return '/api/v1/question/?user=' + id;
      },

      // Site
      getContact: '/api/v1/contact/',
      getBanner: '/api/v1/header/'
    })
    .constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      sessionTimeout: 'auth-session-timeout',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
      all: '*',
      admin: 'admin',
      buyer: 'B',
      seller: 'S'
    })
    .constant('PAGINATION', {
      amount: 9 
    });

})();
