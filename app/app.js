(function () {
  'use strict';

  angular.module('mlaApp', [
    'ui.bootstrap',
    'ui.router',
    'ui.select',
    'ngSanitize',
    'ngCookies',
    'ngMessages',
    'LocalStorageModule',
    'angular-loading-bar',
    'ngAnimate',
    'slickCarousel'
  ])
  .directive('fileread', function () {
    return {
      scope: {
        fileread: '='
      },
      link: function (scope, element) {
        element.bind('change', function (changeEvent) {
          var reader = new FileReader();

          reader.onload = function (loadEvent) {
            scope.$apply(function () {
              scope.fileread = loadEvent.target.result;
            });
          };
          reader.readAsDataURL(changeEvent.target.files[0]);
        });
      }
    };
  })
  .filter('range', function () {
    return function (input, total) {
      total = parseInt(total);

      for (var i = 0; i < total; i++) {
        input.push(i);
      }

      return input;
    };
  })
  .filter('startFrom', function () {
    return function (input, start) {
      start = +start; //parse to int
      return input ? input.slice(start) : [];
    };
  });

})();