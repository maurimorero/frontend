(function () {
'use strict';

  angular
    .module('mlaApp')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = [
    '$state',
    '$window',
    '$location',
    'UserService',
    'ProductService',
    'SessionService',
    'localStorageService'
  ];

  function ProfileController($state, $window, $location, UserService, ProductService, SessionService, localStorageService) {
   let vm = this;

    vm.update = update;
    vm.updatePreferences = updatePreferences;
    vm.getActivity = getActivity;
    vm.dataLoading = false;
    vm.enableMP = enableMP;
    vm.active = 0;

    vm.status = {
      error: '',
      success: false
    };

    // angular ui calendar configuration
    vm.calendar = {
      opened: false,
      format: 'dd-MMMM-yyyy',
      dateOptions: {
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(1920, 5, 22),
        startingDay: 1
      },
      open() {
        this.opened = true;
      }
    };

    // TODO: pull this info from the backend
    vm.provinces = [
      'Buenos Aires',
      'Catamarca',
      'Chaco',
      'Chubut',
      'Córdoba',
      'Corrientes',
      'Entre Ríos',
      'Formosa',
      'Jujuy',
      'La Pampa',
      'La Rioja',
      'Mendoza',
      'Misiones',
      'Neuquén',
      'Río Negro',
      'Salta',
      'San Juan',
      'San Luis',
      'Santa Cruz',
      'Santa Fe',
      'Santiago del Estero',
      'Tierra del Fuego',
      'Tucumán'
    ];

    init();

    ////////////////

    function init() {
      // cleanToken();
      getUser();
      getActivity();
      getPreferences();
      getUserRating();

      const offerMP = localStorageService.get('offerMP');
      const productMP = localStorageService.get('productMP');
      const enableMP = localStorageService.get('enableMP');
      const offerCreate = localStorageService.get('offerCreate');
      const offerBuy = localStorageService.get('offerBuy');
      const quotationCreate = localStorageService.get('quotationCreate');
      const productOfferMP = localStorageService.get('productOfferMP');

      if (offerMP) {
        vm.active = 2;
        localStorageService.remove('offerMP');
      } else if (offerCreate) {
        vm.active = 2;
      } else if (enableMP) {
        vm.active = 2;
        localStorageService.remove('enableMP');
      } else if (quotationCreate) {
        vm.active = 1;
      } else if (productMP) {
        vm.active = 2;
      } else if (offerBuy) {
        vm.active = 1;
        localStorageService.remove('offerBuy');
      } else if (productOfferMP) {
        vm.active = 2;
        localStorageService.remove('productOfferMP');
      }
    }

    function getUser() {
      UserService.getUser().then(user => {
        vm.user = user;

        if (vm.user.is_authorized_by_ml) {
          vm.user.mp = 'Y';
        } else {
          vm.user.mp = 'N';
        }
      });
    }

    function getPreferences() {
      ProductService.getCategories()
        .then(categories => {
          vm.categories = categories;
          return UserService.getUserPreferences();
        })
        .then(preferences => {
          vm.preferences = preferences.categories;
        });
    }

    // Profile
    function update() {
      vm.dataLoading = true;

      // Quotation create
      const quotationCreate = localStorageService.get('quotationCreate');

      // Continue adding offer case
      const offerData = localStorageService.get('offerCreate') || {};
      const continueOfferId = offerData.quotationId;
      const custom = offerData.custom;

      UserService.updateUser(vm.user.exportData())
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';

          // Continue adding offer
          if (continueOfferId) {
            localStorageService.remove('offerCreate');

            $state.go('quotationsAddOffer', {
              custom: custom ? 'custom' : 'generic',
              quotationId: continueOfferId
            });
          }

          if (quotationCreate) {
            if (_.get(quotationCreate, 'product.id')) {
              $state.go('quotationsCreate', {id: quotationCreate.product.id});
            } else {
              $state.go('quotationsCreate');
            }
          }
        })
        .catch(() => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = 'Error al actualizar los datos';
        });
    }

    // Preferences
    function updatePreferences() {
      vm.dataLoading = true;

      const preferences = {
        user_id: SessionService.getUserId(),
        category_ids: vm.preferences.map(({id}) => id)
      };

      UserService.updateUserPreferences(preferences)
        .then(() => {
          vm.dataLoading = false;
          vm.status.success = true;
          vm.status.error = '';
        })
        .catch(err => {
          vm.dataLoading = false;
          vm.status.success = false;
          vm.status.error = err.data && err.data.error;
        });
    }

    // Activity
    function getActivity() {
      UserService.getUserActivity()
        .then(activity => {
          vm.activity = {
            lastLogin: moment(activity.last_login).format('YYYY-MM-DD'),
            lastPurchase: activity.last_purchase ? moment(activity.last_purchase).format('YYYY-MM-DD') : '-',
            purchasesCount: activity.purchases_count || 0,
            salesCount: activity.sales_count || 0,
            sugnup: activity.signup
          };
        });
    }

    function getUserRating() {
      UserService.getUserRating(UserService.get)
        .then(rating => vm.rating = rating);
    }

    function validateMP() {
      return UserService.getUser().then(data => {
        if (!data.is_authorized_by_ml) {
          localStorageService.set('enableMP', true);
          $window.location.replace(data.url_authorization_ml);
        }
      });
    }

    // function cleanToken() {
    //   const search = new URL($location.absUrl());
    //   $window.location.replace(`${search.origin}${search.pathname}`);
    // }

    function enableMP() {
      vm.user.mp = vm.user.mp === 'Y' ? 'N' : 'Y';
      if (vm.user.mp === 'Y') {
        validateMP();
      } else if (vm.user.mp === 'N') {
        UserService.disableMP();
      }
    }

  }
})();