(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('UserModel', UserModel);

  function UserModel() {

    const userPrototype = {
      setData,
      exportData
    };

    return User;

    ////////////////////

    /**
     * @param  {Object} data (json data from the server)
     */
    function setData(userData) {
      let user = this;

      const getValidFields = userData => {
        var validFields = [
          'date_joined',
          'area_code',
          'birthdate',
          'city',
          'cuit',
          'document',
          'first_name',
          'floor',
          'gender',
          'last_name',
          'phone',
          'street',
          'street_number',
          'unit',
          'email',
          'url_authorization_ml',
          'is_authorized_by_ml',
          'cuit_facturation',
          'city_facturation',
          'street_facturation',
          'street_number_facturation',
          'floor_facturation',
          'unit_facturation',
          'tax_type_facturation',
          'business_name_facturation'
        ];

        return _.pick(userData, validFields);
      };

      const formatDates = userData => {
        userData.birthdate = moment(userData.birthdate).toDate();
        userData.date_joined = moment(userData.date_joined).format('YYYY-MM-DD');
        return userData;
      };

      const processUserData = _.flow(formatDates, getValidFields);

      angular.extend(user, processUserData(userData));
    }

    /**
     * Exports the data that'll be sent to the server
     */
    function exportData() {
      const user = angular.copy(this);

      user.birthdate = moment(user.birthdate).format('YYYY-MM-DD');

      if (user.tax_type_facturation === 'F') {
        user.business_name_facturation = '';
        user.cuit_facturation = 0;
      }

      delete user.date_joined;

      return angular.toJson(user);
    }

    /**
     * User constructor function
     * @param  {Object} data
     * @returns {Object}
     */
    function User(data) {
      let user = Object.create(userPrototype);
      data && user.setData(data);

      return user;
    }
  }
})();
