(function () {
'use strict';

  angular
    .module('mlaApp')
    .factory('UserService', UserService);

    UserService.$inject = ['$http', '$q', 'API', 'SessionService', 'UserModel'];

    function UserService($http, $q, API, SessionService, UserModel) {

      var service = {
        getUser,
        createUser,
        updateUser,
        updateUserPreferences,
        getUserPreferences,
        getUserActivity,
        getUserRating,
        disableMP
      };

      return service;

      ///////////////////////////////////

      /**
       * Gets the user information and returns
       * a user model instance
       */
      function getUser(id) {
        const deferred = $q.defer();
        // TODO: create a specic method for the seller user
        const userId = id ? id : SessionService.getUserId();

        $http.get(API.getUser(userId))
          .then(function (response) {
            deferred.resolve(UserModel(response.data));
          })
          .catch(function (err) {
            deferred.reject(err);
          });

        return deferred.promise;
      }

      /**
       * @param  {Object} userData
       */
      function createUser(userData) {
        return $http.post(API.register, userData);
      }

      /**
       *  Updates the user information
       *
       *  @param {Object} userData
       */
      function updateUser(userData) {
        const id = SessionService.getUserId();
        return $http.post(API.updateUser(id), userData);
      }

      /**
       * Updates the user preferences
       */
      function updateUserPreferences(preferences) {
        return $http.post(API.updateUserPreferences, preferences);
      }

      /**
       * Gets the user preferences
       */
      function getUserPreferences() {
        const deferred = $q.defer();

        $http.get(API.getUserPreferences)
          .then(response => {
            deferred.resolve(response.data);
          })
          .catch(err => {
            deferred.reject(err);
          });

        return deferred.promise;
      }

      /**
       * Gets the user activity
       */
      function getUserActivity() {
        const deferred = $q.defer();

        $http.get(API.getUserActivity)
          .then(response => deferred.resolve(response.data))
          .catch(err => deferred.reject(err));

        return deferred.promise;
      }

      /**
       * Gets the user activity
       */
      function getUserRating(id) {
        const deferred = $q.defer();
        const userId = id ? id : SessionService.getUserId();

        $http.get(API.getUserRating(userId))
          .then(response => deferred.resolve(response.data))
          .catch(err => deferred.reject(err));

        return deferred.promise;
      }

      function disableMP() {
        const deferred = $q.defer();

        $http.get(API.disableMP)
          .then(response => deferred.resolve(response.data))
          .catch(err => deferred.reject(err));

        return deferred.promise;
      }
    }
})();
