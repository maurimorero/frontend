(function () {
'use strict';

  // Usage: <rating points="0..5"></rating>
  //
  // Creates:
  //

  angular
    .module('mlaApp')
    .component('rating', {
      templateUrl: 'app/users/views/user.rating.component.view.html',
      controller: RatingController,
      bindings: {
        points: '<'
      }
    });

  function RatingController() {
    const ctrl = this;
  }
})();
