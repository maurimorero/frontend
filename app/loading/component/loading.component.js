(function () {
'use strict';

  angular
    .module('mlaApp')
    .component('loading', {
      templateUrl: 'app/loading/views/loading.component.view.html',
      controller: LoadingController,
      bindings: {
        show: '<'
      }
    });

  function LoadingController() {
    // const ctrl = this;

    ////////////////

    // ctrl.$onInit = function () {};
    // $ctrl.$onChanges = function(changesObj) { };
    // $ctrl.$onDestroy = function() { };
  }
})();