(function () {
'use strict';

  // Usage: <header></header>
  //
  // Creates: navigation bar
  //

  angular
    .module('mlaApp')
    .component('header', {
      templateUrl: 'app/home/views/header.view.html',
      controller: HeaderController,
      bindings: {}
    });

  HeaderController.$inject = ['$state', '$rootScope', '$window', '$location', 'SessionService', 'AuthenticationService', 'AUTH_EVENTS'];

  function HeaderController($state, $rootScope, $window, $location, SessionService, AuthenticationService, AUTH_EVENTS) {
    const ctrl = this;

    ctrl.logout = logout;
    ctrl.$onInit = onInit;
    ctrl.switchRole = switchRole;
    ctrl.goToInitRolePage = goToInitRolePage;
    ctrl.getInitPage = getInitPage;
    ctrl.userRole = {
      type: SessionService.getUserRole()
    };

    ctrl.isHome = true;

    if ($location.$$url !== '/home') {
      ctrl.isHome = false;
    }

    $rootScope.$on('$stateChangeStart', function (e, toState) {
      ctrl.isHome = toState.url === '/home';
      ctrl.userRole.type = SessionService.getUserRole();
    });

    ////////////////

    function onInit() {
      ctrl.loggedIn = SessionService.userLoggedIn();
      ctrl.username = SessionService.getUsername();

      $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 300) {
          $('.sticker').removeClass('stick');
        }else{
          $('.sticker').addClass('stick');
        }
      });

      setTimeout(() => {
        $('.main-menu').meanmenu({
          meanScreenWidth: '991',
          meanMenuContainer: '.mobile-menu',
          meanMenuClose: '<i class="glyphicon glyphicon-remove-circle"></i>',
          meanMenuOpen: '<i class="glyphicon glyphicon-align-justify"></i>',
          meanRevealPosition: 'right',
          meanMenuCloseSize: '30px'
        });
      });
    }

    $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
      ctrl.$onInit();
    });

    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
      ctrl.loggedIn = '';
      ctrl.username = '';
    });

    function logout() {
      AuthenticationService.logout()
        .then(() => $state.go('home'));
    }

    function goToInitRolePage() {
      if (ctrl.userRole.type === 'buyer') {
        $state.go('productFind');
      } else {
        $state.go('quotations');
      }
    }

    function switchRole() {
      if (ctrl.userRole.type === 'buyer') {
        $state.go('quotations');
        SessionService.setUserRole('seller');
        $rootScope.$broadcast('seller');
        ctrl.userRole = {
          name: 'Vendedor',
          type: 'seller'
        };
      } else {
        $state.go('productFind');
        SessionService.setUserRole('buyer');
        $rootScope.$broadcast('buyer');
        ctrl.userRole = {
          name: 'Comprador',
          type: 'buyer'
        };
      }
    }

    function getInitPage() {
      return ctrl.userRole.type === 'seller' ? 'quotations' : 'home';
    }
  }
})();
