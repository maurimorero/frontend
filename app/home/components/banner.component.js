(function () {
  'use strict';

    // Usage: <banner></banner>
    //
    // Creates: navigation bar
    //

    angular
      .module('mlaApp')
      .component('banner', {
        templateUrl: 'app/home/views/banner.view.html',
        controller: BannerController,
        bindings: {}
      });

    BannerController.$inject = ['$rootScope', '$location', 'HomeService'];

    function BannerController($rootScope, $location, HomeService) {
      const ctrl = this;

      ctrl.$onInit = onInit;
      ctrl.isHome = true;

      if ($location.$$url !== '/home') {
        ctrl.isHome = false;
      }

      $rootScope.$on('$stateChangeStart', function (e, toState) {
        ctrl.isHome = toState.url === '/home';
      });

      ////////////////

      function onInit() {
        HomeService.getBannerInfo()
          .then(info => ctrl.info = info);
      }
    }
  })();