(function () {
  'use strict';

    // Usage: <banner></banner>
    //
    // Creates: navigation bar
    //

    angular
      .module('mlaApp')
      .component('headerimage', {
        templateUrl: 'app/home/views/header-image.view.html',
        controller: HeaderImageController,
        bindings: {}
      });

    HeaderImageController.$inject = ['$rootScope', '$location', 'HomeService'];

    function HeaderImageController($rootScope, $location, HomeService) {
      const ctrl = this;

      ctrl.$onInit = onInit;
      ctrl.isHome = true;

      if ($location.$$url !== '/home') {
        ctrl.isHome = false;
      }

      $rootScope.$on('$stateChangeStart', function (e, toState) {
        ctrl.isHome = toState.url === '/home';
      });

      ////////////////

      function onInit() {
        HomeService.getBannerInfo()
          .then(info => ctrl.info = info);
      }
    }
  })();
