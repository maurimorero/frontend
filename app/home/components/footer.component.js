(function () {
  'use strict';

    // Usage: <footer></footer>
    //
    // Creates: navigation bar
    //

    angular
      .module('mlaApp')
      .component('footer', {
        templateUrl: 'app/home/views/footer.view.html',
        controller: FooterController,
        bindings: {}
      });

    FooterController.$inject = ['HomeService'];

    function FooterController(HomeService) {
      const ctrl = this;

      ctrl.$onInit = onInit;

      ////////////////

      function onInit() {
        HomeService.getContactInfo()
          .then(info => ctrl.info = info);
      }
    }
  })();