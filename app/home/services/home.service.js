(function () {
  'use strict';

    angular
      .module('mlaApp')
      .factory('HomeService', HomeService);

    HomeService.$inject = ['$http', '$q', 'API'];

    function HomeService($http, $q, API) {
      const service = {
        getContactInfo,
        getBannerInfo
      };

      return service;

      ////////////////

      function getContactInfo() {
        const deferred = $q.defer();

        $http.get(API.getContact)
          .then(response => deferred.resolve(_.first(response.data.objects)));

        return deferred.promise;
      }

      function getBannerInfo() {
        const deferred = $q.defer();

        $http.get(API.getBanner)
          .then(response => deferred.resolve(_.first(response.data.objects)));

        return deferred.promise;
      }
    }
  })();